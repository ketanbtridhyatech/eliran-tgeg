<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class GeneralSettings extends Authenticatable
{
   
    use SoftDeletes;
    protected $table = 'general_settings';

}
?>
