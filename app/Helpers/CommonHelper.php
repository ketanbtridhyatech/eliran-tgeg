<?php
namespace App\Helpers;
use App\GeneralSettings;
use Auth;
use DB;

class CommonHelper {
    /**
     * @param int $user_id User-id
     * 
     * @return string
     */
    public static function getSettingData() {
        $getData =  GeneralSettings::first();
        return $getData;
    }
}