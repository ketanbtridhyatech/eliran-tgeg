<?php

namespace App\Http\Controllers\Auth;

use App\GeneralSettings;
use App\Http\Controllers\Controller;
use App\Role;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use GuzzleHttp\Client;
use App\RoleUser;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */

    protected $redirectTo = '/dashboard';


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    public function login(Request $request)
    {


        if (isset($request['sToken']) && $request['sToken'] != '') {

            $getIp = $this->getIp();
            //$getIp = '103.106.20.165';
            $user = User::where('s_token', $request['sToken'])->first();
            //if (!empty($user) && in_array($getIp, explode(',', $user->ip))) {
            if(!empty($user)){
                Auth::logout();
                Auth::loginUsingId($user->id, true);
            }
        }
        $route = \Request::route()->getName();


        if ($route == "company_list" && $request['token'] != '') {
            $getIp = $this->getIp();
            //$getIp = '103.107.26.6';
            //$external_user = User::where('first_name','קבוצת')->first();
            $user = User::where('s_token', $request['token'])->first();
            //if (!empty($user) && in_array($getIp, explode(',', $user->ip))) {
                if(!empty($user)){
                $data = \Request::all();
                $role_user = RoleUser::where('user_id',$user->id)->first();
                Auth::logout();
                if($role_user->role_id == 2 || $role_user->role_id == '2' ){
                    $check_user = User::where('external_agent_id',$data['agent_id'])->first();
                    if(empty($check_user)){
                       
                        $client = new Client([
                            'headers' => ['Content-Type' => 'application/x-www-form-urlencoded']
                        ]);
                        $param=array();
                        $param['key'] = '3765d732472d44469e70a088caef3040';
                        $param['acc_id'] = 3326;
                        $param['user_id'] = $data['agent_id'];
                        
                        $response = $client->request('POST', 'http://proxy.leadim.xyz/apiproxy/acc3305/getuser.ashx', [
                            'form_params' => $param,
                            'exceptions' => true,
                            '_token' => csrf_token(),
                
                        ]);
                        $response = json_decode($response->getBody()->getContents());
                        if($response->status == 'success'){
                            $user = new User();
                            $user->first_name = $response->result->name;
                            $user->email = $response->result->email;
                            $user->company_id = $role_user->user_id;
                            $user->external_agent_id = $data['agent_id'];
                            $user->save();

                            $check_role = RoleUser::where('user_id',$user->id)->first();
                            if(empty($check_role)){
                                $user_role = new RoleUser();
                                $user_role->user_id=$user->id;
                                $user_role->role_id=4;
                                $user_role->save();
                            }
                            Auth::login($user);
                        }else{
                            $user = new User();
                            $user->first_name = $data['agent_id'];
                            $user->company_id = $role_user->user_id;
                            $user->external_agent_id = $data['agent_id'];
                            $user->save();

                            $check_role = RoleUser::where('user_id',$user->id)->first();
                            if(empty($check_role)){
                                $user_role = new RoleUser();
                                $user_role->user_id=$user->id;
                                $user_role->role_id=4;
                                $user_role->save();
                            }
                            Auth::login($user);
                        }
                    }else{
                        Auth::login($check_user);
                    }
                }
                else{
                    Auth::login($user);
                }
                
                return redirect()->route('company_list', ['first_name' => isset($data['first_name']) ? $data['first_name'] : '', 'social_id' => isset($data['social_id']) ? $data['social_id'] : '', 'mobile_number' => isset($data['mobile_number']) ? $data['mobile_number'] : '', 'm_code' => isset($data['m_code']) ? $data['m_code'] : '', 'year' => isset($data['year']) ? $data['year'] : '', 'month' => isset($data['month']) ? $data['month'] : '', 'day' => isset($data['day']) ? $data['day'] : '', 'record_id' => isset($data['record_id']) ? $data['record_id'] : '', 'agent_id' => isset($data['agent_id']) ? $data['agent_id'] : '', 'login_with_token' => 1]);
            }
        }
        $this->validateLogin($request);

        if (
            method_exists($this, 'hasTooManyLoginAttempts') &&
            $this->hasTooManyLoginAttempts($request)
        ) {
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }
    public function getIp()
    {
        foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key) {
            if (array_key_exists($key, $_SERVER) === true) {
                foreach (explode(',', $_SERVER[$key]) as $ip) {
                    $ip = trim($ip); // just to be safe
                    if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false) {
                        return $ip;
                    }
                }
            }
        }
    }
    protected function credentials(Request $request)
    {
        $credentials = [
            $this->username() => strtolower($request->get($this->username())),
            "password" => $request->get("password")
        ];

        return $credentials;
    }
    public function clear_browser_instance(Request $request){
        $settings = GeneralSettings::first();
        $client = new Client([
            'headers' => ['Content-Type' => 'application/json']
        ]);


        $response = $client->request('GET',$settings->python_url.'clear_browser_instance'
        , [
            'body' => '',
            'exceptions' => false,
            '_token' => csrf_token(),

        ]);
        /*echo "<Pre>";
        $response = json_decode($response->getBody()->getContents());
        print_r($response);
        exit;*/
    }
}
