<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Session\Session;
use Webpatser\Uuid\Uuid;
use GuzzleHttp\Client;
use Illuminate\Auth\Access\Response;
use App\ActivityLog;
use App\GeneralSettings;
use App\HitsCounter;
use App\RoleUser;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

ini_set('max_execution_time', '0');


class CrawlersController extends Controller
{


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {



        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function get_otp(Request $request)
    {
        $settings = GeneralSettings::first();
        $log = new ActivityLog();
        $log->agent_id = Auth::user()->id;
        $log->datetime = date('Y-m-d h:i:s');
        $log->action_desc = "System try to login in " . $request->company_name;
        $log->on_id = $request->social_id;
        $log->save();

        $user_settings_data = User::where('id', Auth::user()->company_id)->first();

        $uuid = Uuid::generate()->string;
        $request->session()->put('company_session.' . $request->company_name, $uuid);
        $param = array();
        $param['user_uuid'] = $request->session()->get('company_session')[$request->company_name];
        $param['id_type'] = '0';
        $param['id_num'] = $request->social_id;
        $param['login_code_type'] = '0';
        $param['mobile_number'] = $request->mobile_number;
        $param['company_id'] = isset($user_settings_data->id) ? (string) $user_settings_data->id : '';
        $param['bucket_name'] = isset($user_settings_data->s3_bucket_name) ? $user_settings_data->s3_bucket_name : '';
        $client = new Client([
            'headers' => ['Content-Type' => 'application/json']
        ]);
        $company_name = $request->company_name == 'clal' ? 'clalbit' : $request->company_name;

        /*  if ($request->company_name == "phoenix" || $request->company_name == "clalbit" ) {*/
        $response = $client->request('POST', $settings->python_url . $company_name . '/login', [
            'body' => json_encode($param),
            'exceptions' => false,
            '_token' => csrf_token(),

        ]);
        /*}*/
        $mobile = $request->mobile_number;

        $response = json_decode($response->getBody()->getContents());
        $log = new ActivityLog();
        $log->agent_id = Auth::user()->id;
        $log->datetime = date('Y-m-d h:i:s');
        $log->action_desc = "System try to login in " . $company_name;
        $log->on_id = $request->social_id;
        $log->json_data = json_encode($response, JSON_UNESCAPED_UNICODE);
        $log->save();

        if (!empty($response) && $response->status == 1) {
            $hit_counter = new HitsCounter();
            $hit_counter->process_id = $request->process_id;
            $hit_counter->agent_id = $request->agent_id;
            $hit_counter->user_id = Auth::user()->id;
            $hit_counter->description = "Try to login";
            $hit_counter->company_name = $request->company_name;
            $hit_counter->datetime = date('Y-m-d h:i:s');
            $hit_counter->row_response = json_encode($response, JSON_UNESCAPED_UNICODE);
            $hit_counter->crawler_result  = 1;
            $hit_counter->social_id = $request->social_id;
            $hit_counter->mobile_number = $request->mobile_number;
            $hit_counter->user_uuid = $request->session()->get('company_session')[$request->company_name];
            $hit_counter->company_id = isset($user_settings_data->id) ? (string) $user_settings_data->id : '';
            $hit_counter->bucket_name = isset($user_settings_data->s3_bucket_name) ? $user_settings_data->s3_bucket_name : '';
            $hit_counter->save();

            echo View('crawlers.show_otp_textbox', compact('response', 'mobile', 'company_name'))->render();
        } else {
            $hit_counter = new HitsCounter();
            $hit_counter->process_id = $request->process_id;
            $hit_counter->agent_id = $request->agent_id;
            $hit_counter->user_id = Auth::user()->id;
            $hit_counter->description = "Try to login";
            $hit_counter->company_name = $request->company_name;
            $hit_counter->datetime = date('Y-m-d h:i:s');
            $hit_counter->row_response = json_encode($response, JSON_UNESCAPED_UNICODE);
            $hit_counter->crawler_result  = 0;
            $hit_counter->social_id = $request->social_id;
            $hit_counter->mobile_number = $request->mobile_number;
            $hit_counter->user_uuid = $request->session()->get('company_session')[$request->company_name];
            $hit_counter->company_id = isset($user_settings_data->id) ? (string) $user_settings_data->id : '';
            $hit_counter->bucket_name = isset($user_settings_data->s3_bucket_name) ? $user_settings_data->s3_bucket_name : '';
            $hit_counter->save();
            $error_image = isset($response->error_screen_shot) ? $response->error_screen_shot : '';
            $convert_to_array = explode(',', $response->message);
            echo $response->status . "###" . $convert_to_array[0] . "###" . $error_image;
        }
    }
    public function verify_otp(Request $request)
    {
        set_time_limit(0);
        $settings = GeneralSettings::first();
        $log = new ActivityLog();
        $log->agent_id = Auth::user()->id;
        $log->datetime = date('Y-m-d h:i:s');
        $log->action_desc = "System login to the site " . $request->company_name;
        $log->on_id = $request->social_id;
        $log->save();

        $user_settings_data = User::where('id', Auth::user()->company_id)->first();

        $param = array();
        $param['user_uuid'] =  $request->uuid;
        $param['otp_code'] = $request->otp;
        $param['company_id'] = isset($user_settings_data->id) ? (string) $user_settings_data->id : '';
        $param['bucket_name'] = isset($user_settings_data->s3_bucket_name) ? $user_settings_data->s3_bucket_name : '';
        $client = new Client([
            'headers' => ['Content-Type' => 'application/json']
        ]);

        $response = $client->request('POST', $settings->python_url . $request->company_name . '/verify-otp', [
            'body' => json_encode($param),
            'exceptions' => false,
            '_token' => csrf_token(),

        ]);

        $log = new ActivityLog();
        $log->agent_id = Auth::user()->id;
        $log->datetime = date('Y-m-d h:i:s');
        $log->action_desc = "System login to the site " . $request->company_name;
        $log->on_id = $request->social_id;
        $log->json_data = json_encode($response, JSON_UNESCAPED_UNICODE);
        $log->save();

        $response = json_decode($response->getBody()->getContents());

        /*if ($response->status == 1) {
            echo "success";
        } else {
            echo "fail";
        }*/
        if ($response->status == 1 || $response->status == '1') {
            $hit_counter = new HitsCounter();
            $hit_counter->process_id = $request->process_id;
            $hit_counter->agent_id = $request->agent_id;
            $hit_counter->user_id = Auth::user()->id;
            $hit_counter->description = "Verify OTP";
            $hit_counter->company_name = $request->company_name;
            $hit_counter->datetime = date('Y-m-d h:i:s');
            $hit_counter->row_response = json_encode($response, JSON_UNESCAPED_UNICODE);
            $hit_counter->crawler_result  = 1;
            $hit_counter->social_id = $request->social_id;
            $hit_counter->user_uuid = $request->uuid;
            $hit_counter->company_id = isset($user_settings_data->id) ? (string) $user_settings_data->id : '';
            $hit_counter->bucket_name = isset($user_settings_data->s3_bucket_name) ? $user_settings_data->s3_bucket_name : '';
            $hit_counter->save();
            echo "success";
        } else {
            $hit_counter = new HitsCounter();
            $hit_counter->process_id = $request->process_id;
            $hit_counter->agent_id = $request->agent_id;
            $hit_counter->user_id = Auth::user()->id;
            $hit_counter->description = "Verify OTP";
            $hit_counter->company_name = $request->company_name;
            $hit_counter->datetime = date('Y-m-d h:i:s');
            $hit_counter->row_response = json_encode($response, JSON_UNESCAPED_UNICODE);
            $hit_counter->crawler_result  = 0;
            $hit_counter->social_id = $request->social_id;
            $hit_counter->user_uuid = $request->uuid;
            $hit_counter->company_id = isset($user_settings_data->id) ? (string) $user_settings_data->id : '';
            $hit_counter->bucket_name = isset($user_settings_data->s3_bucket_name) ? $user_settings_data->s3_bucket_name : '';
            $hit_counter->save();
            $error_image = isset($response->error_screen_shot) ? $response->error_screen_shot : '';
            $convert_to_array = explode(',', $response->message);
            echo $response->status . "###" . $convert_to_array[0] . "###" . $error_image;
        }
    }
    public function get_files(Request $request)
    {
        $settings = GeneralSettings::first();
        $log = new ActivityLog();
        $log->agent_id = Auth::user()->id;
        $log->datetime = date('Y-m-d h:i:s');
        $log->action_desc = "Login successfully done ! Work in progress for Downloading neccessary files and making zip for " . $request->company_name;
        $log->on_id = $request->social_id;
        $log->json_data = '';
        $log->save();

        $user_settings_data = User::where('id', Auth::user()->company_id)->first();

        $param = array();

        $param['user_uuid'] =  $request->uuid;
        $param['company_id'] = isset($user_settings_data->id) ? (string) $user_settings_data->id : '';
        $param['bucket_name'] = isset($user_settings_data->s3_bucket_name) ? $user_settings_data->s3_bucket_name : '';
        $client = new Client([
            'headers' => ['Content-Type' => 'application/json']
        ]);
        $response = $client->request('POST', $settings->python_url . $request->company_name . '/download_policies', [
            'body' => json_encode($param),
            'exceptions' => false,
            '_token' => csrf_token(),

        ]);
        $response = json_decode($response->getBody()->getContents());
        $log = new ActivityLog();
        $log->agent_id = Auth::user()->id;
        $log->datetime = date('Y-m-d h:i:s');
        $log->action_desc = "Login successfully done ! Work in progress for Downloading neccessary files and making zip for " . $request->company_name;
        $log->on_id = $request->social_id;
        $log->json_data = json_encode($response, JSON_UNESCAPED_UNICODE);
        $log->save();
        if ($response->status == 1 || $response->status == '1') {
            $hit_counter = new HitsCounter();
            $hit_counter->process_id = $request->process_id;
            $hit_counter->agent_id = $request->agent_id;
            $hit_counter->user_id = Auth::user()->id;
            $hit_counter->description = "Download Files";
            $hit_counter->company_name = $request->company_name;
            $hit_counter->datetime = date('Y-m-d h:i:s');
            $hit_counter->row_response = json_encode($response, JSON_UNESCAPED_UNICODE);
            $hit_counter->crawler_result  = 1;
            $hit_counter->social_id = $request->social_id;
            $hit_counter->user_uuid = $request->uuid;
            $hit_counter->company_id = isset($user_settings_data->id) ? (string) $user_settings_data->id : '';
            $hit_counter->bucket_name = isset($user_settings_data->s3_bucket_name) ? $user_settings_data->s3_bucket_name : '';
            $hit_counter->save();

            $file_name = $response->data->policies_file_zip_path;
            $response->data->s_record_id = $request->record_id;
            $response->data->agent_id = $request->agent_id;
            //$response->data->company_id = config('constant.company_code.'.$request->company_name);
            $response->data->response = array('file' => $response->data->policies_file_zip_path, 'company_id' => config('constant.company_code.' . $request->company_name));
            $response->data->procedure_ss = $response->procedure_ss;
            unset($response->data->policies_file_zip_path);
            unset($response->message);
            unset($response->procedure_ss);
            //$settings_data = GeneralSettings::first();
            if (!empty(Auth::user()->company_id)) {
                $user_settings_data = User::where('id', Auth::user()->company_id)->first();
                if (!empty($user_settings_data->webhook_url)) {
                    $client_webhook = new Client([
                        'headers' => ['Content-Type' => 'application/json']
                    ]);
                    $response_webhook = $client_webhook->request('POST', $user_settings_data->webhook_url, [
                        'body' => json_encode($response),
                        'exceptions' => false,
                        '_token' => csrf_token(),

                    ]);
                }
            }


            echo $file_name;
        } else {
            $hit_counter = new HitsCounter();
            $hit_counter->process_id = $request->process_id;
            $hit_counter->agent_id = $request->agent_id;
            $hit_counter->user_id = Auth::user()->id;
            $hit_counter->description = "Download Files";
            $hit_counter->company_name = $request->company_name;
            $hit_counter->datetime = date('Y-m-d h:i:s');
            $hit_counter->row_response = json_encode($response, JSON_UNESCAPED_UNICODE);
            $hit_counter->crawler_result  = 0;
            $hit_counter->social_id = $request->social_id;
            $hit_counter->user_uuid = $request->uuid;
            $hit_counter->company_id = isset($user_settings_data->id) ? (string) $user_settings_data->id : '';
            $hit_counter->bucket_name = isset($user_settings_data->s3_bucket_name) ? $user_settings_data->s3_bucket_name : '';
            $hit_counter->save();
            $error_image = isset($response->error_screen_shot) ? $response->error_screen_shot : '';
            $convert_to_array = explode(',', $response->message);
            echo $response->status . "###" . $convert_to_array[0] . "###" . $error_image;
        }
    }
    public function money_mountain_zip_download(Request $request)
    {
        $uuid = Uuid::generate();
        $request->session()->put('uuid', $uuid);
        $param = array();
        $day = date('j', strtotime($request->social_id_issue_date));
        $month = date('n', strtotime($request->social_id_issue_date));
        $year = date('Y', strtotime($request->social_id_issue_date));
        $param['user_uuid'] = $request->session()->get('uuid')->string;
        $param['id_num'] = $request->social_id;
        $param['issue_day'] = (string) $day;
        $param['issue_month'] = (string) $month;
        $param['issue_year'] = (string) $year;
        $param['sign_in'] = $request->force_login;
        $client = new Client([
            'headers' => ['Content-Type' => 'application/json']
        ]);

        $response = $client->request('POST', config('constant.api_url') . 'money_mountain/login_and_download_files', [
            'body' => json_encode($param),
            'exceptions' => false,
            '_token' => csrf_token(),

        ]);

        $response = json_decode($response->getBody()->getContents());
        if ($response->status == 1 || $response->status == '1') {

            echo $response->data->file_path;
        } else {
            echo $response->status . "###" . $response->message;
        }
    }
    public function insurance_mountain_process(Request $request)
    {
        
        $settings = GeneralSettings::first();
        $log = new ActivityLog();
        $log->agent_id = Auth::user()->id;
        $log->datetime = date('Y-m-d h:i:s');
        $log->action_desc = "Sign in and find affiliate insurance companies";
        $log->on_id = $request->social_id;
        $log->save();

        $user_settings_data = User::where('id', Auth::user()->company_id)->first();

        $uuid = Uuid::generate();
        $request->session()->put('company_session.' . 'insurance_mountain', $uuid);
        $request->session()->put('uuid', $uuid);
        $param = array();
        $param['user_uuid'] = $request->session()->get('uuid')->string;
        $param['id_num'] = $request->social_id;
        $param['issue_day'] = (string) $request->day;
        $param['issue_month'] = (string) $request->month;
        $param['issue_year'] = (string) $request->year;
        $param['company_id'] = isset($user_settings_data->id) ? (string) $user_settings_data->id : '';
        $param['bucket_name'] = isset($user_settings_data->s3_bucket_name) ? $user_settings_data->s3_bucket_name : '';

        $client = new Client([
            'headers' => ['Content-Type' => 'application/json']
        ]);


        $response = $client->request('POST', $settings->python_url . 'insurance_mountain/login', [
            'body' => json_encode($param),
            'exceptions' => false,
            '_token' => csrf_token(),

        ]);

        $response = json_decode($response->getBody()->getContents());


        $sites = '';
        if ($response->status == 1 || $response->status == '1') {
            $ins_companies = array(
                'migdal' => 'מגדל',
                'clalbit' => 'כלל',
                'menora' => 'מנורה',
                'phoenix' => 'הפניקס',
                'harel' => 'הראל',
                'ayalon' => 'איילון',
                'haschara' => 'הכשרה',
                'yashir' => 'איי. די. איי',
                'nine_million' => 'איי. די. איי',
                'aig' => 'איי אי גי',
            );
            $str = array();
            if (!empty($response->data->companies)) {
                foreach ($response->data->companies as $comp) {
                    foreach ($ins_companies as $key => $value) {

                        if (strpos($comp, $value) !== false) {
                            $str[] = $key;
                        }
                    }
                }
            }
            $hit_counter = new HitsCounter();
            $hit_counter->process_id = $request->process_id;
            $hit_counter->agent_id = $request->agent_id;
            $hit_counter->user_id = Auth::user()->id;
            $hit_counter->description = "Finding Company for insurance mountain";
            $hit_counter->company_name = 'Insurance Mountain';
            $hit_counter->datetime = date('Y-m-d h:i:s');
            $hit_counter->row_response = json_encode($response, JSON_UNESCAPED_UNICODE);
            $hit_counter->crawler_result  = 1;
            $hit_counter->social_id = $request->social_id;
            $hit_counter->user_uuid = $request->session()->get('uuid')->string;
            $hit_counter->company_id = isset($user_settings_data->id) ? (string) $user_settings_data->id : '';
            $hit_counter->bucket_name = isset($user_settings_data->s3_bucket_name) ? $user_settings_data->s3_bucket_name : '';
            $hit_counter->save();

            $file_name = $response->data->file;
            $response->data->s_record_id = $request->record_id;
            $response->data->agent_id = $request->agent_id;
            //$response->data->company_id = config('constant.company_code.'.$request->company_name);
            $response->data->response = array('file' => $response->data->file, 'company_id' => config('constant.company_code.' . $request->company_name));
            $response->data->procedure_ss = $response->procedure_ss;
            unset($response->data->file);
            unset($response->data->companies);
            unset($response->message);
            unset($response->procedure_ss);
            //$settings_data = GeneralSettings::first();
            if (!empty(Auth::user()->company_id)) {
                $user_settings_data = User::where('id', Auth::user()->company_id)->first();
                if (!empty($user_settings_data->webhook_url)) {
                    $client_webhook = new Client([
                        'headers' => ['Content-Type' => 'application/json']
                    ]);
                    $response_webhook = $client_webhook->request('POST', $user_settings_data->webhook_url, [
                        'body' => json_encode($response),
                        'exceptions' => false,
                        '_token' => csrf_token(),

                    ]);
                }
            }
            echo implode(',', $str) . "&&&&&" . $file_name;
        } else {
            $hit_counter = new HitsCounter();
            $hit_counter->process_id = $request->process_id;
            $hit_counter->agent_id = $request->agent_id;
            $hit_counter->user_id = Auth::user()->id;
            $hit_counter->description = "Finding Company for insurance mountain";
            $hit_counter->company_name = 'Insurance Mountain';
            $hit_counter->datetime = date('Y-m-d h:i:s');
            $hit_counter->row_response = json_encode($response, JSON_UNESCAPED_UNICODE);
            $hit_counter->crawler_result  = 0;
            $hit_counter->social_id = $request->social_id;
            $hit_counter->user_uuid = $request->session()->get('uuid')->string;
            $hit_counter->company_id = isset($user_settings_data->id) ? (string) $user_settings_data->id : '';
            $hit_counter->bucket_name = isset($user_settings_data->s3_bucket_name) ? $user_settings_data->s3_bucket_name : '';
            $hit_counter->save();
            $error_image = isset($response->error_screen_shot) ? $response->error_screen_shot : '';
            $convert_to_array = explode(',', $response->message);
            echo $response->status . "###" . $convert_to_array[0] . "###" . $error_image;
        }
    }
    function show_companies(Request $request)
    {

        if ($request->session()->has('old_social_id')) {
        } else {
            $request->session()->put('old_social_id', '1');
        }
        $data = $request->all();

        $count = HitsCounter::orderBy('id', 'desc')->first();

        //$old_social_id=$data['social_id'];

        if (empty($count)) {
            $process_id = 1;
            $request->session()->put('old_social_id', $data['social_id']);
            $request->session()->put('process_id', $process_id);
            $hit_counter = new HitsCounter();
            $hit_counter->process_id = $process_id;
            $hit_counter->agent_id = isset($data['agent_id']) ? $data['agent_id'] : NULL;
            $hit_counter->user_id = Auth::user()->id;
            $hit_counter->description = "submited client form";
            $hit_counter->datetime = date('Y-m-d h:i:s');
            $hit_counter->save();
        } else if ($request->session()->get('old_social_id') != $data['social_id']) {
            $process_id = $count->id + 1;
            $request->session()->put('old_social_id', $data['social_id']);
            $request->session()->put('process_id', $process_id);
            $hit_counter = new HitsCounter();
            $hit_counter->process_id = $process_id;
            $hit_counter->agent_id = isset($data['agent_id']) ? $data['agent_id'] : NULL;
            $hit_counter->user_id = Auth::user()->id;
            $hit_counter->description = "submited client form";
            $hit_counter->datetime = date('Y-m-d h:i:s');
            $hit_counter->save();
        }
        return View('new_client', compact('data'));
    }
    public function billing_report(Request $request)
    {
        $user_ids = RoleUser::whereIn('role_id', array('3', '4'))->pluck('user_id');
        $users = User::whereIn('id', $user_ids)->get()->toArray();
        $companies_role_user = RoleUser::where('role_id', 2)->pluck('user_id');
        $companies = User::whereIn('id', $companies_role_user)->get()->toArray();
        foreach ($users as $key => $value) {
            $users[$key]['total_hits'] = HitsCounter::where('user_id', $value['id'])->count();
            $users[$key]['success_hit'] = HitsCounter::where('user_id', $value['id'])->where('crawler_result', 1)->where('description', 'Download Files')->count();
            $user_company = !empty($value['company_id']) ? User::find($value['company_id']) : '';
            $users[$key]['company_name'] = !empty($user_company) ? $user_company['first_name'] . " " . $user_company['last_name'] : '';
        }
        return View('hit_counter_report', compact('users', 'companies'));
    }
    public function billing_report_filter(Request $request)
    {
        if (!empty($request->user)) {
            $user_ids = RoleUser::whereIn('role_id', array('3', '4'))->where('user_id', $request->user)->pluck('user_id');
        } else {
            $user_ids = RoleUser::whereIn('role_id', array('3', '4'))->pluck('user_id');
        }

        if (!empty($request->company)) {
            $users = User::whereIn('id', $user_ids)->where('company_id', $request->company)->get()->toArray();
        } else {
            $users = User::whereIn('id', $user_ids)->get()->toArray();
        }

        $companies_role_user = RoleUser::where('role_id', 2)->pluck('user_id');
        $companies = User::whereIn('id', $companies_role_user)->get()->toArray();
        foreach ($users as $key => $value) {
            if (!empty($request->date_range)) {
                $dates = explode('-', $request->date_range);
                $users[$key]['total_hits'] = HitsCounter::where('user_id', $value['id'])->whereBetween(DB::raw('DATE(datetime)'), array(date('Y-m-d', strtotime($dates[0] . '-' . $dates[1] . '-' . $dates[2])), date('Y-m-d', strtotime($dates[3] . '-' . $dates[4] . '-' . $dates[5]))))->count();
                $users[$key]['success_hit'] = HitsCounter::where('user_id', $value['id'])->where('crawler_result', 1)->where('description', 'Download Files')->whereBetween(DB::raw('DATE(datetime)'), array(date('Y-m-d', strtotime($dates[0] . '-' . $dates[1] . '-' . $dates[2])), date('Y-m-d', strtotime($dates[3] . '-' . $dates[4] . '-' . $dates[5]))))->count();
            } else {
                $users[$key]['total_hits'] = HitsCounter::where('user_id', $value['id'])->count();
                $users[$key]['success_hit'] = HitsCounter::where('user_id', $value['id'])->where('crawler_result', 1)->where('description', 'Download Files')->count();
            }

            $user_company = !empty($value['company_id']) ? User::find($value['company_id']) : '';
            $users[$key]['company_name'] = !empty($user_company) ? $user_company['first_name'] . " " . $user_company['last_name'] : '';
        }
        echo View('filter_hit_counter_report', compact('users', 'companies'))->render();
    }
    public function send_mail(Request $request)
    {
        $settings_data = GeneralSettings::first();
        $ins_companies = array(
            'migdal' => 'Migdal',
            'clalbit' => 'Clal',
            'menora' => 'Menora',
            'phoenix' => 'Phoenix',
            'harel' => 'Harel',
            'ayalon' => 'Ayalon',
            'haschara' => 'Hachshara',
            'yashir' => 'Bituach Yashir',
            'nine_million' => '9,000,000',
            'aig' => 'AIG',
            'insurance_mountain' => 'Insurance Mountain'
        );
        //echo "<Pre>";
       // print_r($request->session()->get('company_session'));
        //exit;
        $uuid = isset($request->session()->get('company_session')[$request->company_name])?$request->session()->get('company_session')[$request->company_name]:"";
        $user_settings_data = User::where('id', Auth::user()->company_id)->first();
        $param = array();
        $param['user_uuid'] = $request->company_name == 'insurance_mountain' ? $request->session()->get('uuid')->string : $uuid;
        $param['date'] = date('F') . "-" . date('d') . "-" . date('Y');
        $param['company_id'] = isset($user_settings_data->id) ? (string) $user_settings_data->id : '';
        $param['bucket_name'] = isset($user_settings_data->s3_bucket_name) ? $user_settings_data->s3_bucket_name : '';
        $param['site_name'] = $request->company_name;
        $settings = GeneralSettings::first();
        $client = new Client([
            'headers' => ['Content-Type' => 'application/json']
        ]);


        $response = $client->request(
            'POST',
            $settings->python_url . 'get_log_file',
            [
                'body' => json_encode($param),
                'exceptions' => false,
                '_token' => csrf_token(),

            ]
        );
        $response = json_decode($response->getBody()->getContents());


        $log_file_url = '';
        if ($response->status == 1 || $response->status == '1') {
            $log_file_url = $response->data;
        }


        $company_name = $ins_companies[$request->company_name];
        $data = array('company_name' => $company_name, 'error_msg' => $request->description, 'social_id' => $request->social_id, 'agent_id' => Auth::user()->id, 'agent_name' => Auth::user()->first_name . " " . Auth::user()->last_name, 'log_file_url' => $log_file_url, 'error_img_url' => $request->error_image);
        Mail::send('email.mail', $data, function ($message) use ($settings_data) {
            $message->to($settings_data->email)->subject('דיווח תקלה במערכת איזיפול');
            $message->from('bugs@ezdigital.co.il');
        });
    }
    public function investigations_log(Request $request)
    {


        $ids = HitsCounter::select(DB::raw('max(id) as id'))->whereNotNull('user_uuid')->groupBy('user_uuid')->pluck('id');
        $inv_log = HitsCounter::whereIn('id', $ids)->orderBy('id', 'DESC')->offset(0)->limit(1000)->get();
        return View('investigations_log', compact('inv_log'));
    }
    public function investigations_log_filter(Request $request)
    {
        $ids = HitsCounter::select(DB::raw('max(id) as id'))->whereNotNull('user_uuid')->groupBy('user_uuid')->pluck('id');
        $inv_log = HitsCounter::whereIn('id', $ids);

        if (!empty($request->date)) {
            $inv_log->whereDate('datetime', date('Y-m-d', strtotime($request->date)));
        }
        if (!empty($request->uuid)) {
            $inv_log->where('user_uuid', $request->uuid);
        }
        if (!empty($request->social_id)) {
            $inv_log->where('social_id', $request->social_id);
        }
        if ($request->status != '') {

            $inv_log->where('crawler_result', $request->status);
        }
        $inv_log = $inv_log->orderBy('id', 'DESC')->offset(0)->limit(1000)->get();
        echo View('filter_investigations_log', compact('inv_log'))->render();
    }
    public function print_json(Request $request)
    {
        $inv_log = HitsCounter::find($request->id);
        $param = array();
        $param['user_uuid'] = $inv_log->user_uuid;
        $param['date'] = date('F', strtotime($inv_log->datetime)) . "-" . date('d', strtotime($inv_log->datetime)) . "-" . date('Y', strtotime($inv_log->datetime));
        $param['company_id'] = (string) $inv_log->company_id;
        $param['bucket_name'] = $inv_log->bucket_name;
        if ($inv_log->company_name == 'Insurance Mountain') {
            $param['site_name'] = 'insurance_mountain';
        } else {
            $param['site_name'] = $inv_log->company_name;
        }

        $settings = GeneralSettings::first();
        $client = new Client([
            'headers' => ['Content-Type' => 'application/json']
        ]);


        $response = $client->request(
            'POST',
            $settings->python_url . 'get_log_file',
            [
                'body' => json_encode($param),
                'exceptions' => false,
                '_token' => csrf_token(),

            ]
        );
        $response = json_decode($response->getBody()->getContents());

        $log_file_url = '';
        if ($response->status == 1 || $response->status == '1') {
            /*$fs = Storage::getDriver();
            $stream = $fs->readStream($response->data);
            echo \Response::stream(function() use($stream) {
                fpassthru($stream);
            }, 200, [
                "Content-Type" => $fs->getMimetype($response->data),
                "Content-Length" => $fs->getSize($response->data),
                "Content-disposition" => "attachment; filename=\"" .basename($response->data) . "\"",
                ]);*/

            return response()->stream(function () use ($response) {
                //Can add logic to chunk the file here if you want but the point is to stream data
                readfile($response->data);
            }, 200, [
                "Content-Type" => "application/json",
                "Content-Disposition" => "attachment; filename=\"test.json\""
            ]);
        } else {
            echo " ";
        }
    }
}
