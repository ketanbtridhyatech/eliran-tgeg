<?php

namespace App\Http\Controllers;

use App\Permission;
use App\Role;
use App\RolePermission;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Session\Session;
use App\HitsCounter;
use Illuminate\Support\Facades\DB;

use App\Log;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $users = User::count();
        if (Auth::user()->hasRole('Agent')) {

            $ins_companies = array();
            $request->session()->put('company_session', $ins_companies);
            return view('client_info', compact('users'));
        }
        $todayDate = date("Y-m-d");
        $getTotalLogs = Log::leftJoin('users', 'logs.agent_id', 'users.id')
        ->where('logs.datetime', 'like', '%' . $todayDate . '%')
        ->select('users.first_name', 'users.last_name', 'logs.datetime', DB::raw('COUNT(*) as total_logs'))
        ->groupBy('logs.on_id')->get();
        
        return view('index', compact('users','getTotalLogs'));
    }

    public function chartData()
    {
        $todayDate = date("Y-m-d");
        $to_date = date('Y-m-d', strtotime("-15 days")); 
        //        $todayDate = '2019-11-15';
        $hitsData = HitsCounter::leftJoin('users', 'hits_counter.user_id', 'users.id')
            ->whereBetween(DB::raw('DATE(hits_counter.datetime)'), array($to_date,$todayDate))
            ->select('users.first_name', 'users.last_name', 'hits_counter.datetime','hits_counter.user_id', DB::raw('count(*) as total_hits'))
            ->groupBy(DB::raw('DATE(hits_counter.datetime)'))->get();
        $result = array();
        if (!$hitsData->isEmpty()) {
            foreach ($hitsData as $key => $val) {
                $result['user_id'][] = $val->user_id;
                $result['user_name'][] = date('d-m-Y',strtotime($val->datetime));
                $result['user_hits'][] = $val->total_hits;
            }
        }
        $successHits = HitsCounter::leftJoin('users', 'hits_counter.user_id', 'users.id')
            ->where('hits_counter.description', 'Download Files')
            ->where('hits_counter.crawler_result', 1)
            ->whereBetween(DB::raw('DATE(hits_counter.datetime)'), array($to_date,$todayDate))
            ->select('users.first_name','hits_counter.datetime', 'users.last_name', 'hits_counter.user_id', DB::raw('count(*) as total_hits'))
            ->groupBy(DB::raw('DATE(hits_counter.datetime)'))->get();
        if (!$successHits->isEmpty()) {
            foreach ($successHits as $key => $val) {
                $result['sHit_user_id'][] = $val->user_id;
                $result['sHit_user_name'][] = date('d-m-Y',strtotime($val->datetime));
                $result['sHit_user_hits'][] = $val->total_hits;
            }
        }
        return $result;
    }
    public function logout()
    {
        Auth::logout();
        return redirect('/login');
    }
    public function change_language(Request $request)
    {
        App::setLocale($request->lang);
        $request->session()->put('locale', $request->lang);
    }
    public function permission_settings(Request $request)
    {
        $roles = Role::get();
        $permission = Permission::get();
        $rp = array();
        $role_permission = RolePermission::get();
        if (!empty($role_permission)) {
            foreach ($role_permission as $rper) {
                $rp[] = $rper->permission_id . "__" . $rper->role_id;
            }
        }
        return view('permission_settings', compact('roles', 'permission', 'rp'));
    }
    public function add_remove_permission(Request $request)
    {
        $role_id = $request->role_id;
        $permission = $request->permission_id;
        $check_role_permission = RolePermission::where('role_id', $role_id)->where('permission_id', $permission)->first();
        if (empty($check_role_permission)) {
            $role_permission = new RolePermission;
            $role_permission->role_id = $request->role_id;
            $role_permission->permission_id = $request->permission_id;
            $role_permission->create_by = Auth::user()->id;
            $role_permission->save();
        } else {
            RolePermission::where('role_id', $role_id)->where('permission_id', $permission)->delete();
        }
    }

    public function newClient()
    {
        return view('client_info');
    }
    public function client_info_page()
    {
        return view('client_info');
    }
    public function hit_counter_report(Request $request){
        $todayDate = date('Y-m-d');
        $yesterday = date('Y-m-d', strtotime("-1 days"));

        $monday = strtotime("last monday");
        $monday = date('w', $monday) == date('w') ? $monday + 7 * 86400 : $monday;
        $sunday = strtotime(date("Y-m-d", $monday) . " +6 days");
        $this_week_sd = date("Y-m-d", $monday);
        $this_week_ed = date("Y-m-d", $sunday);

        $tday = "01";
        $tmonth = date("m");
        $tyear = date("Y");
        $month_sd = date("Y-m-d", strtotime($tmonth.'/'.$tday.'/'.$tyear.' 00:00:00'));
        $month_ed = date("Y-m-d", strtotime('-1 second',strtotime('+1 month',strtotime($tmonth.'/'.$tday.'/'.$tyear.' 00:00:00'))));
        
        $last_monday = strtotime("last monday");
        $last_monday = date('W', $last_monday) == date('W') ? $last_monday - 7 * 86400 : $last_monday;
        $last_sunday = strtotime(date("Y-m-d", $last_monday) . " +6 days");
        $last_week_sd = date("Y-m-d", $last_monday);
        $last_week_ed = date("Y-m-d", $last_sunday);

        if($request->type == 'today'){
        $date = $todayDate;
        $getTotalLogs = HitsCounter::leftJoin('users', 'hits_counter.user_id', 'users.id')
            ->where('hits_counter.datetime', 'like', '%' . $todayDate . '%')
            ->select('users.first_name', 'users.last_name', 'hits_counter.datetime', DB::raw('COUNT(hits_counter.process_id) as total_logs'))
            ->groupBy(DB::raw('DATE(hits_counter.datetime)'),'hits_counter.user_id')->get();
        }else if($request->type == 'yesterday'){
            $date = $yesterday;
            $getTotalLogs = HitsCounter::leftJoin('users', 'hits_counter.user_id', 'users.id')
            ->where('hits_counter.datetime', 'like', '%' . $yesterday . '%')
            ->select('users.first_name', 'users.last_name', 'hits_counter.datetime', DB::raw('COUNT(hits_counter.process_id) as total_logs'))
            ->groupBy(DB::raw('DATE(hits_counter.datetime)'),'hits_counter.user_id')->get();
        }else if($request->type == 'this_week'){
            $date = $this_week_sd ." - ".$this_week_ed;
            $getTotalLogs = HitsCounter::leftJoin('users', 'hits_counter.user_id', 'users.id')
            ->whereBetween(DB::raw('DATE(hits_counter.datetime)'), array($this_week_sd,$this_week_ed))
            ->select('users.first_name', 'users.last_name','users.id','hits_counter.datetime', DB::raw('count(hits_counter.process_id) as total_logs'))
            ->groupBy(DB::raw('DATE(hits_counter.datetime)'),'hits_counter.user_id')->get();
        }else if($request->type == 'this_month'){
            $date = $month_sd ." - ".$month_ed;
            $getTotalLogs = HitsCounter::leftJoin('users', 'hits_counter.user_id', 'users.id')
            ->whereBetween(DB::raw('DATE(hits_counter.datetime)'), array($month_sd,$month_ed))
            ->select('users.first_name', 'users.last_name', 'hits_counter.datetime', DB::raw('COUNT(hits_counter.process_id) as total_logs'))
            ->groupBy(DB::raw('DATE(hits_counter.datetime)'),'hits_counter.user_id')->get();
        }else if($request->type == 'last_week'){
            $date = $last_week_sd ." - ".$last_week_ed;
            $getTotalLogs = HitsCounter::leftJoin('users', 'hits_counter.user_id', 'users.id')
            ->whereBetween(DB::raw('DATE(hits_counter.datetime)'), array($last_week_sd,$last_week_ed))
            ->select('users.first_name', 'users.last_name', 'hits_counter.datetime', DB::raw('COUNT(hits_counter.process_id) as total_logs'))
            ->groupBy(DB::raw('DATE(hits_counter.datetime)'),'hits_counter.user_id')->get();
        }
        echo view('hit_counter_report_dashborad',compact('date','getTotalLogs'));
    }
}
