<?php

namespace App\Http\Controllers;

use App\GeneralSettings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Session\Session;
use Webpatser\Uuid\Uuid;
use GuzzleHttp\Client;
use Illuminate\Auth\Access\Response;


class SettingsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function update_settings(Request $request)
    {
        $settings = GeneralSettings::first();
        return View('settings.general_settings', compact('settings'));
    }

    public function store_setting(Request $request)
    {

        $validatedData = $request->validate([
            'logoUpload' => 'mimes:jpeg,jpg,png',
            'title_name' => 'required',
            //'callback_return_url' => 'required'
        ]);

        if (isset($request['logoUpload'])) {
            $file = $request->file('logoUpload');
            $extension = $file->getClientOriginalExtension();
            $filename = 'logo-light-text' . time() . '.' . $extension;
            $destinationPath = public_path() . '/assets/images';
            $file->move($destinationPath, $filename);
        }

        $settingCheck = GeneralSettings::first();
        if (empty($settingCheck)) {
            $settingObje = new GeneralSettings();
        } else {

            $settingObje = $settingCheck;
            if (isset($request['logoUpload'])) {
                if (file_exists($destinationPath . '/' . $settingObje['site_logo'])) {
                    \File::delete($destinationPath . '/' . $settingObje['site_logo']);
                }
            }
        }
        if (isset($request['logoUpload'])) {
            $settingObje->site_logo = $filename;
        }

        $settingObje->updated_by = Auth::user()->id;
        $settingObje->title = $request['title_name'];
        // $settingObje->callback_return_url = $request['callback_return_url'];
        $settingObje->python_url = $request['python_url'];
        $settingObje->email = $request['email'];
        $settingObje->captcha_service = isset($request->set_captcha) ? $request->set_captcha : Null;
        $settingObje->save();

        if (isset($request->set_captcha)) {
            $settings = GeneralSettings::first();

            $client = new Client([
                'headers' => ['Content-Type' => 'application/json']
            ]);


            $response = $client->request(
                'GET',
                $settings->python_url . 'get_captcha_services',
                [
                    'body' => '',
                    'exceptions' => false,
                    '_token' => csrf_token(),

                ]
            );
            $response = json_decode($response->getBody()->getContents());
            $params = array();
            if (!empty($response) && ($response->status == 1 || $response->status == '1')) {
                foreach ($response->data as $res) {

                    if ($res->service_name == $request->set_captcha) {
                        $params['service_id'] = $res->service_id;
                        //$params['min_balance'] = $res->min_balance;
                        $params['active_status'] = true;
                    }
                }
                    $client_set = new Client([
                        'headers' => ['Content-Type' => 'application/json']
                    ]);


                    $response_set = $client_set->request(
                        'POST',
                        $settings->python_url . 'set_captcha_service',
                        [
                            'body' => json_encode($params),
                            'exceptions' => false,
                            '_token' => csrf_token(),

                        ]
                    );
                    $response_set = json_decode($response_set->getBody()->getContents());
              
                    if ($response_set->status == 1 || $response_set->status == '1') {
                    } else {
                        $request->session()->flash('error', 'Something Went Wrong !');
                        return redirect('/general_setting_page');
                    }
                }
            } else {
                $request->session()->flash('error', 'Something Went Wrong !');
                return redirect('/general_setting_page');
            }
        
        $request->session()->flash('success', 'Settings Update Successfully');
        return redirect('/general_setting_page');
    }
}
