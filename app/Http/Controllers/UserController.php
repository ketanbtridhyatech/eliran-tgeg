<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Session\Session;
use App\User;
use App\RoleUser;
use App\Role;
use Illuminate\Support\Str;
use GuzzleHttp\Client;
use App\GeneralSettings;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    { 
        $users = User::get();
        foreach($users as $key=>$value){
            if(!empty($value->company_id)){
                $company_name = User::find($value->company_id);
                if(!empty($company_name)){
                    $users[$key]->company_name = $company_name->first_name." ".$company_name->last_name;
                    $users[$key]->company_token = $company_name->s_token;
                }else{
                    $users[$key]->company_name = '';
                    $users[$key]->company_token = '';
                }

            }else{
                $users[$key]->company_name = '';
            }
        }
      
        return view('users.index',compact('users'));
        
    }
    public function add_new_user(){
        $roles=Role::get();
        $company_role_id = Role::where('name','Company')->first();
        $company_role_user = RoleUser::where('role_id',$company_role_id->id)->pluck('user_id');
        $company_role_user_details = User::whereIn('id',$company_role_user)->get();
        return view('users.add_user',compact('roles','company_role_user_details'));
    }
    public function store_user_details(Request $request){
        $settings = GeneralSettings::first();
        //$data=$request->all();
      
        if(empty($request->user_id)){
            $validatedData = $request->validate([
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required|email:rfc',
                'password' => 'required',
                'confirm_password' => 'required',
                'role'=>'required'
            ]);
            $user = new User();
            $user->password = bcrypt($request->password);
            $user->s_token = Str::random(60);
            $user->ip = $this->getIp();
        }else{
            $validatedData = $request->validate([
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required|email:rfc',
                'role'=>'required'
            ]);
            $user = User::find($request->user_id);
            $user->s_token = $request->secure_token;
            $user->ip = $request->ip_address;
        }
       if($request->role == '2' || $request->role == 2){
        $user->s3_bucket_name =$request->s3_bucket_name;
        $user->s3_access_key=$request->s3_access_key;
        $user->s3_secret_key=$request->s3_secret_key;
        $user->ftp_host=$request->ftp_host;
        $user->ftp_password=$request->ftp_password;
        $user->ftp_port=$request->ftp_port;
        $user->ftp_username=$request->ftp_username;
        $user->ftp_public_url=$request->ftp_public_url;
        $user->webhook_url=$request->webhook_url;

       }
       if($request->role == 3 || $request->role == '3'){
           $user->company_id = $request->company_id;
       }
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = strtolower($request->email); 
        $user->phone_number = $request->phone_number;
       
        $user->save();        
        
        $check_role = RoleUser::where('user_id',$user->id)->first();
        if(empty($check_role)){
            $user_role = new RoleUser();
            $user_role->user_id=$user->id;
            $user_role->role_id=$request->role;
            $user_role->create_by = Auth::user()->id;
            $user_role->save();
        }else{
            $check_role->role_id = $request->role;
            $check_role->update_by = Auth::user()->id;
            $check_role->save();
        }
        $company_role_id = Role::where('name','Company')->first();
        $company_role_user = RoleUser::where('role_id',$company_role_id->id)->pluck('user_id');
        $company_role_user_details = User::select('id','s3_access_key','s3_secret_key')->whereIn('id',$company_role_user)->get();
        $company_array = array();
      
        foreach($company_role_user_details as $cr_details){
            
            $company_array[0][(string)$cr_details->id] =array('access_key'=>$cr_details->s3_access_key,'secret_key'=>$cr_details->s3_secret_key);
            
        }
        $client_update_key = new Client([
            'headers' => ['Content-Type' => 'application/json']
        ]);
        $client_update_key = $client_update_key->request('POST', $settings->python_url.'edit_config', [
            'body' => json_encode($company_array[0]),
            'exceptions' => false,
            '_token' => csrf_token(),

        ]);
        if(empty($request->user_id)){
            $request->session()->flash('success', 'New User Added');
        }else{
            $request->session()->flash('success', 'User Updated Successfully');
        }
        
        return redirect('/users');
    
    }
    
    public function delete_user(Request $request){
        User::where('id',$request->user_id)->delete();
    }
    public function edit_user(Request $request){
        $user_details = User::find($request->id);
        $user_role=RoleUser::where('user_id',$user_details->id)->pluck('role_id')->toArray();
        $action="edit";
        $roles=Role::get();
        $company_role_id = Role::where('name','Company')->first();
        $company_role_user = RoleUser::where('role_id',$company_role_id->id)->pluck('user_id');
        $company_role_user_details = User::whereIn('id',$company_role_user)->get();
        return view('users.add_user',compact('user_details','action','roles','user_role','company_role_user_details'));
    }
    public function getIp(){
        foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key){
            if (array_key_exists($key, $_SERVER) === true){
                foreach (explode(',', $_SERVER[$key]) as $ip){
                    $ip = trim($ip); // just to be safe
                    if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false){
                        return $ip;
                    }
                }
            }
        }
    }
    
}
