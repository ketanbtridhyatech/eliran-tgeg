<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class RoleUser extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    protected $table = 'role_user';

}
?>
