<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    //protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function roles()
    {
      
        $user_role = RoleUser::where('user_id',Auth::user()->id)->pluck('role_id');
        return Role::whereIn('id',$user_role)->get();
    }
    
    public function permission()
    {
      
        $user_permission = RoleUser::leftJoin('role_permission as rp','role_user.role_id','rp.role_id')
                            ->where('role_user.user_id',Auth::user()->id)->pluck('rp.permission_id');
        return Permission::whereIn('id',$user_permission)->get();
    }
    
    public function hasRole($role)
    {
       
        foreach ($this->roles() as $role_data)
        {
            
            if ($role_data->name == $role)
            {
                
                return true;
            }
        }

        return false;
    }
    
     public function hasPermission($permission)
    {
       
        foreach ($this->permission() as $permission_data)
        {
            
            if ($permission_data->name == $permission)
            {
                
                return true;
            }
        }

        return false;
    }
}
