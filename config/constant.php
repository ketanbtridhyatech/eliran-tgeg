<?php

return [
    'api_url' => 'http://104.248.193.233:5001/',
    'company_code'=>[
        'migdal' => 1,
        'clalbit'  => 2,
        'menora' => 3,
        'phoenix'  => 4,
        'harel'  => 5,
        'ayalon'  => 6,
        'hachshara'  => 7,
        'yashir'  => 8,
        'nine_million'=> 9,
        'aig' => 10,
        'insurance_mountain' => 11,
    ]
];
