$(function () {
    "use strict";
    $('#social_id_report_type').trigger('change');
    
});

$('#social_id_issue_date').datepicker({
    format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true
});

function sparklineHitCart(){
    $.ajax({
        url: base_url + "/dashboard/chart_data",
        type: 'get',
        success: function(response) { 
            console.log(response);
            var offsethtml = new Array();
            var totalHits  =   0;
            for(var i = 0 ;i<response.user_hits.length;i++){
                offsethtml.push(response.user_name[i]);
                totalHits += parseInt(response.user_hits[i]);
            }
            $('.hiteCount').html(totalHits);
            var obj = offsethtml.reduce(function(o,key,val) { o[val] = key; return o; }, {});
            $('#sparkline2dash').sparkline(response.user_hits, {
                type: 'bar',
                tooltipFormat: '{{offset:offset}} : {{value}}',
                tooltipValueLookups: {
                    'offset': obj
                },
                height: '154',
                barWidth: '4',
                resize: true,
                barSpacing: '10',
                barColor: '#25a6f7'
            });
            
            var successHitOffset = new Array();
            var successTotalHits = 0 ;
            for(var i = 0 ;i<response.sHit_user_hits.length;i++){
                successHitOffset.push(response.sHit_user_name[i]);
                successTotalHits += parseInt(response.sHit_user_hits[i]);
            }
            $('.successHitTotal').html(successTotalHits);
            var successObj = successHitOffset.reduce(function(o,key,val) { o[val] = key; return o; }, {});
            $('#sales1').sparkline(response.sHit_user_hits, {
                type: 'bar',
                tooltipFormat: '{{offset:offset}} : {{value}}',
                tooltipValueLookups: {
                    'offset': successObj
                },
                height: '154',
                barWidth: '4',
                resize: true,
                barSpacing: '10',
                barColor: '#25a6f7'
            });
        }
    });
}

// sparkline
var sparklineLogin = function () {


};
var sparkResize,sparkResizeHitChart;
$(window).resize(function (e) {
    clearTimeout(sparkResize);
    clearTimeout(sparkResizeHitChart);
    sparkResize = setTimeout(sparklineLogin, 500);
//    sparkResizeHitChart = setTimeout(sparklineHitCart, 500);
});
sparklineLogin();
sparklineHitCart();
// ==============================================================
// code for open popup when click on company in dashboard
// ==============================================================
$('#policy_details').validate({ // initialize the plugin
    rules: {
        social_id: {
            required: true,

        },
        mobile_number: {
            required: true,

        },
    },
    messages: {
        social_id: "Please enter Socialid.",
        mobile_number: "Please enter mobile",

    },

});
$("#mobile_number").on("keypress keyup blur", function (e) {
    var regex = new RegExp(/^(\?\+?[0-9]\?)?[0-9_\-+ ]*$/);
    var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);

    if (regex.test(str)) {
        return true;
    } else {
        e.preventDefault();
        return false;
    }

});
$('#social_id_report_type').on('change',function(){
    $.ajax({
        url: base_url + "/dashboard/hit_counter_report",
        type: 'post',
        data:{
            type:$(this).val()
        },
        success: function(response) { 
            $('#response_data').html(response)
        }
    })

})
