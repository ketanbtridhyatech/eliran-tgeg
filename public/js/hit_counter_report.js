$(function () {
    "use strict";
    $('#user_list').DataTable({
        responsive: true,
        "columns": [
            null,
            null,
            null,
            null,
            null,
            null,
            
        ],
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excelHtml5',
                filename: 'User Hits Report',
                text :i18n.lan_constant.export_to_excel,
                title:'',
                
            },
        ],
        "bDestroy": true
    });
    $('#date_range').daterangepicker({
       
        locale: {
            format: 'DD-MM-YYYY',
            cancelLabel: 'Clear'
        },
      
        autoUpdateInput:false,
       
        
    });
    $('#date_range').val('');
})
$('#company_name').on('change',function(){
    $.ajax({
        url: base_url + "/hit_counter/filter",
        method: 'post',
        data: {
            date_range: $('#date_range').val(),
            company : $(this).val(),
            user:$('#user_name').val()

        },
        success: function (result) {
         $('#user_list').html(result)
         if($('#date_range').val() == ''){
            $('#user_list').DataTable({
                responsive: true,
                "columns": [
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    
                ],
                dom: 'Bfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        filename: 'User Hits Report',
                        title:'',
                        text :i18n.lan_constant.export_to_excel,
                    },
                ],
                "bDestroy": true
            });
         }else{
            $('#user_list').DataTable({
                responsive: true,
                "columns": [
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    
                ],
                dom: 'Bfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        filename: 'User Hits Report',
                        title:'',
                        text :i18n.lan_constant.export_to_excel,
                        messageTop:i18n.lan_constant.date_range +" : "+ $('#date_range').val()
                    },
                ],
                "bDestroy": true
            });
         }
       
        }
    });
})
$('#date_range').on('apply.daterangepicker', function(ev, picker) {
   
    $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
    $('#company_name').trigger('change');
    
});
$('#date_range').on('cancel.daterangepicker', function(ev, picker) {
    $(this).val('');
    $('#company_name').trigger('change');
});
$('#user_name').on('change',function(){
    $('#company_name').trigger('change');
})

