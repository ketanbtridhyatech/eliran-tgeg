$(function () {
    "use strict";
    $('#log_list').DataTable({
        responsive: true,
        "columns": [
            null,
            null,
            null,
            null,
            null,
            null,
            {
                "orderable": false
            },
                        
        ],
        pageLength:50,
        "bDestroy": true

        
    });
    $('#date').attr('readonly',true);
    $('#date').datepicker({
            format: 'dd-mm-yyyy',
            todayHighlight:true,
            autoclose:true
    }).on('changeDate', function(e) {
       $('#status').trigger('change');
    });
    
})
$('#status').on('change',function(){
    $.ajax({
        url: base_url + "/investigations_log/filter",
        method: 'post',
        data: {
            date: $('#date').val(),
            uuid : $('#uuid').val(),
            social_id:$('#social_id').val(),
            status:$(this).val()

        },
        success: function (result) {
         $('#log_list').html(result)
            $('#log_list').DataTable({
                responsive: true,
                "columns": [
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    {
                        "orderable": false
                    },
                    
                ],
                pageLength:50,
                "bDestroy": true
            });
         
        }
    });
})

$('.text').on('keyup',function(){
    $('#status').trigger('change');
});
$(document).on('click','.show_log',function(){
    $('.preloader').show();
    $.ajax({
        url: base_url + "/investigations_log/print_json",
        method: 'post',
        type:JSON,
        data: {
            id:$(this).attr('id')

        },
        success: function (result) {
            $('.preloader').hide();
            $('#json_data').html( JSON.stringify(result,undefined, 4));
            $('#json_modal').modal();
          
        }
    })
})

