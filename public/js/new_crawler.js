$(function () {

    "use strict";
    $.SweetAlert.init()

    //This is for the Notification top right

    // Dashboard 1 Morris-chart
    /*Morris.Area({
        element: 'morris-area-chart'
        , data: [{
                period: '2010'
                , iphone: 50
                , ipad: 80
                , itouch: 20
        }, {
                period: '2011'
                , iphone: 130
                , ipad: 100
                , itouch: 80
        }, {
                period: '2012'
                , iphone: 80
                , ipad: 60
                , itouch: 70
        }, {
                period: '2013'
                , iphone: 70
                , ipad: 200
                , itouch: 140
        }, {
                period: '2014'
                , iphone: 180
                , ipad: 150
                , itouch: 140
        }, {
                period: '2015'
                , iphone: 105
                , ipad: 100
                , itouch: 80
        }
            , {
                period: '2016'
                , iphone: 250
                , ipad: 150
                , itouch: 200
        }]
        , xkey: 'period'
        , ykeys: ['iphone', 'ipad', 'itouch']
        , labels: ['iPhone', 'iPad', 'iPod Touch']
        , pointSize: 3
        , fillOpacity: 0
        , pointStrokeColors: ['#00bfc7', '#fb9678', '#9675ce']
        , behaveLikeLine: true
        , gridLineColor: '#e0e0e0'
        , lineWidth: 3
        , hideHover: 'auto'
        , lineColors: ['#00bfc7', '#fb9678', '#9675ce']
        , resize: true
    });
    Morris.Area({
        element: 'morris-area-chart2'
        , data: [{
                period: '2010'
                , SiteA: 0
                , SiteB: 0
        , }, {
                period: '2011'
                , SiteA: 130
                , SiteB: 100
        , }, {
                period: '2012'
                , SiteA: 80
                , SiteB: 60
        , }, {
                period: '2013'
                , SiteA: 70
                , SiteB: 200
        , }, {
                period: '2014'
                , SiteA: 180
                , SiteB: 150
        , }, {
                period: '2015'
                , SiteA: 105
                , SiteB: 90
        , }
            , {
                period: '2016'
                , SiteA: 250
                , SiteB: 150
        , }]
        , xkey: 'period'
        , ykeys: ['SiteA', 'SiteB']
        , labels: ['Site A', 'Site B']
        , pointSize: 0
        , fillOpacity: 0.4
        , pointStrokeColors: ['#b4becb', '#01c0c8']
        , behaveLikeLine: true
        , gridLineColor: '#e0e0e0'
        , lineWidth: 0
        , smooth: false
        , hideHover: 'auto'
        , lineColors: ['#b4becb', '#01c0c8']
        , resize: true
    });*/

});

// sparkline
var sparklineLogin = function () {
    $('#sales1').sparkline([20, 40, 30], {
        type: 'pie',
        height: '90',
        resize: true,
        sliceColors: ['#01c0c8', '#7d5ab6', '#ffffff']
    });
    $('#sparkline2dash').sparkline([6, 10, 9, 11, 9, 10, 12], {
        type: 'bar',
        height: '154',
        barWidth: '4',
        resize: true,
        barSpacing: '10',
        barColor: '#25a6f7'
    });

};
var sparkResize;

$(window).resize(function (e) {
    clearTimeout(sparkResize);
    sparkResize = setTimeout(sparklineLogin, 500);
});
sparklineLogin();
// ==============================================================
// code for open popup when click on company in dashboard
// ==============================================================

$("#mobile_number").on("keypress keyup blur", function (e) {
    var regex = new RegExp(/^(\?\+?[0-9]\?)?[0-9_\-+ ]*$/);
    var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);

    if (regex.test(str)) {
        return true;
    } else {
        e.preventDefault();
        return false;
    }

});


$('.get_info').on('click', function () {
    document.getElementById('policy_details').reset();
    $('#enter_otp').hide();
    $('#policy_details').validate().resetForm();
    $('.error').removeClass('error');
    $('#get_policy_data_modal').modal();
    $('#submit_data').hide();
    $('#captcha').html('');
    $('#get_captcha').show();
    $('#otp_message').html('');
    $('#company_name').val($(this).attr('company_name'));
    if ($('#company_name').val() == 'money mountain') {
        //$('.show_astrict').show();
        $('.force_login').show();
        $('#mobile_number_data').hide();
    } else {
        //$('.show_astrict').hide();
        $('.force_login').hide();
        $('#mobile_number_data').show();
    }
    if ($('#company_name').val() == 'insurance mountain' || $('#company_name').val() == 'money mountain') {
        $('#socialid_issue_date_data').show();
        $('.show_astrict').show();
        //$('#mobile_number_data').hide();
    } else {
        $('.show_astrict').hide();
        //$('#mobile_number_data').show();
        $('#socialid_issue_date_data').hide();

    }
})
$('.get_captcha').on('click', function () {
    var company_name = $(this).attr('company_name');
    $('#bug_'+company_name).show();
    if (company_name == 'money mountain') {
        $('.loader__label').html('Login successfully done ! Work in progress for Downloading neccessary files and making zip...');
        $('.preloader').show();
        if ($('#force_login').prop("checked") == true) {
            force_login = true;
        } else {
            force_login = false;
        }
        $.ajax({
            url: base_url + "/money_mountain_zip_download",
            method: 'post',
            data: {
                social_id: $('#social_id').val(),
                social_id_issue_date: $('#social_id_issue_date').val(),
                company_name: $('#company_name').val(),
                force_login: force_login
            },
            success: function (result) {
                var res = result.split("###");
                if (res.length > 1) {
                    $('.preloader').hide();
                    alert(res[1]);
                    location.reload();
                } else {
                    $('#download_link').attr('href', result);
                    $('#get_policy_data_modal').modal('hide')
                    $('#download_popup').modal();
                    $('.preloader').hide();
                }
            }
        })
    } else if (company_name == 'insurance_mountain') {

        if ($('#day').val() == '' || $('#month').val() == '' || $('#year').val() == '') {
            //Swal.fire(i18n.lan_constant.date_require_insurance_mountain);
            $('#myLargeModalLabel').html(i18n.lan_constant.date_require_insurance_mountain);
            $('#fill_date_modal').modal();


            return false;
        }
        $(this).attr("disabled", true);
        $('#show_error_image_' + company_name).hide();
        $('#' + company_name + "_no_file_msg").hide();
        $('#otp_details_' + company_name).hide();
        $('#loader_text_' + company_name).html(i18n.lan_constant.find_insurance_site);
        $('#' + company_name + '_loader').show();
        $.ajax({
            url: base_url + "/insurance_mountain_process",
            method: 'post',
            data: {
                social_id: $('#social_id').val(),
                social_id_issue_date: $('#social_id_issue_date').val(),
                company_name: "insurance_mountain",
                mobile_number: $('#mobile_number').val(),
                day: $('#day').val(),
                month: $('#month').val(),
                year: $('#year').val(),
                agent_id: $('#agent_id').val(),
                process_id: $('#process_id').val(),
                record_id: $('#record_id').val()
            },
            success: function (result) {
                var res = result.split("###");
                if (res.length > 1) {
                    $('#' + company_name + '_loader').hide();
                    $('#otp_details_' + company_name).show();
                    $('#otp_details_' + company_name).html(res[1]);
                    if (res[2] != '') {
                       
                        var image_array = res[2].split(',');
                        var html_data = "";
                        for(var k=0;k<image_array.length;k++){
                            html_data=html_data+"<a href='#' class='show_error_image width-40' error_img='"+image_array[k]+"'><image src='"+base_url+"/assets/icons/image_error_icon.png' class='w-25'></a>";
                        }
                        $('#show_error_image_' + company_name).html(html_data);
                        $('#show_error_image_' + company_name).show();
                    }
                    $('#btn_' + company_name).removeAttr('disabled');
                } else {
                    $('#show_error_image_' + company_name).hide()
                    console.log(result);
                    var file_name = result.split("&&&&&");
                    var comp_array = file_name[0].split(",");
                    $(".get_captcha").each(function (index) {
                        if (jQuery.inArray($(this).attr('company_name'), comp_array) < 0) {
                            $(this).attr("disabled", true);
                        }
                    });
                    if (comp_array.length > 0) {
                        for (var i = 0; i < comp_array.length; i++) {
                            $('#btn_' + company_name).removeAttr('disabled');
                            $('#btn_' + comp_array[i]).click();
                        }
                    }
                    $('#otp_details_' + company_name).show();
                    $('#otp_details_' + company_name).html(file_name[0]);
                    $('#btn_' + company_name).removeAttr('disabled');
                    //$('#' + company_name + "_file_link_excel").show();
                    $('#' + company_name + "_file_link_zip").hide();
                   
                    if (file_name[1]) {
                        $('#' + company_name + "_no_file_msg").hide();
                        $('#' + company_name + "_file_link_excel").show();
                        $('#' + company_name + "_file_link_excel").attr('href', file_name[1]);
                    } else {
                        $('#' + company_name + "_file_link_excel").hide();
                        $('#' + company_name + "_no_file_msg").show();
                        $('#' + company_name + "_no_file_msg").html(i18n.lan_constant.no_file_download);
                    }

                    $('#' + company_name + '_loader').hide();
                }
            }
        })
    } else {
        var company_name = $(this).attr('company_name');

        $(this).attr("disabled", true);
        $('#otp_details_' + company_name).hide();
        $('#loader_text_' + company_name).html(i18n.lan_constant.try_login_msg);
        $('#' + company_name + '_loader').show();
        $('#show_error_image_' + company_name).hide();
        $('#' + company_name + "_no_file_msg").hide();
        $.ajax({
            url: base_url + "/get_otp",
            method: 'post',
            data: {
                social_id: $('#social_id').val(),
                mobile_number: $('#mobile_number').val(),
                company_name: company_name,
                process_id: $('#process_id').val(),
                agent_id: $('#agent_id').val()
            },
            success: function (result) {
                var res = result.split("###");
                if (res.length == 1) {
                    $('#show_error_image_' + company_name).hide();
                    $('#' + company_name + '_loader').hide();
                    $('#otp_details_' + company_name).show();
                    $('#otp_details_' + company_name).html(result);
                    var otp_text = 'otp_' + company_name;

                    $("form[data-form-validate='true']").each(function () {
                        $(this).validate();
                    })

                    $('#submit_data_' + company_name).on('click', function () {
                        var company_name = $(this).attr('data-company-name');
                        if ($('#verify_otp_form_' + company_name).valid()) {
                            $('#otp_details_' + company_name).hide();
                            $('#loader_text_' + company_name).html(i18n.lan_constant.login_msg);
                            $('#' + company_name + '_loader').show();
                            $.ajax({
                                url: base_url + "/verify_otp",
                                method: 'post',
                                data: {
                                    otp: $('#otp_' + company_name).val(),
                                    company_name: $('#company_name_' + company_name).val(),
                                    uuid: $('#uuid_' + company_name).val(),
                                    social_id: $('#social_id').val(),
                                    process_id: $('#process_id').val(),
                                    agent_id: $('#agent_id').val()

                                },
                                success: function (result) {
                                    var res = result.split("###");
                                    if (res.length == 1) {
                                        $('#show_error_image_' + company_name).hide();
                                        //if (result && result != 'fail') {
                                        $('#loader_text_' + company_name).html(i18n.lan_constant.download_msg);
                                        $.ajax({
                                            url: base_url + "/get_files",
                                            method: 'post',
                                            data: {
                                                company_name: $('#company_name_' + company_name).val(),
                                                uuid: $('#uuid_' + company_name).val(),
                                                social_id: $('#social_id').val(),
                                                process_id: $('#process_id').val(),
                                                agent_id: $('#agent_id').val(),
                                                record_id: $('#record_id').val()
                                            },
                                            success: function (result) {
                                                var res = result.split("###");
                                                if (res.length > 1) {
                                                    $('#' + company_name + '_loader').hide();
                                                    $('#otp_details_' + company_name).show();
                                                    $('#otp_details_' + company_name).html(res[1]);
                                                    if (res[2] != '') {
                                                        $('#show_error_image_' + company_name).html("<a href='#' class='show_error_image width-40' error_img='"+res[2]+"'><image src='"+base_url+"/assets/icons/image_error_icon.png'></a>")
                                                        $('#show_error_image_' + company_name).show();
                                                    }
                                                    $('#btn_' + company_name).removeAttr('disabled');
                                                    // location.reload();
                                                } else {
                                                    $('#show_error_image_' + company_name).hide();
                                                    $('#' + company_name + "_file_link_excel").hide();
                                                    
                                                    if(result){
                                                        $('#' + company_name + "_file_link_zip").show();
                                                        $('#' + company_name + "_no_file_msg").hide();
                                                        $('#' + company_name + "_file_link_zip").attr('href', result);
                                                    }else{
                                                        $('#' + company_name + "_no_file_msg").show();
                                                        $('#' + company_name + "_file_link_zip").hide();
                                                        $('#' + company_name + "_no_file_msg").html(i18n.lan_constant.no_file_download);
                                                    }
                                                    $('#' + company_name + "_file_link_zip").attr('href', result);
                                                    $('#' + company_name + '_loader').hide();
                                                    $('#otp_details_' + company_name).show();
                                                    $('#otp_details_' + company_name).html(i18n.lan_constant.success_msg_for_file);
                                                    $('#btn_' + company_name).removeAttr('disabled');
                                                }

                                            }
                                        })
                                    } else {
                                        $('#' + company_name + '_loader').hide();
                                        $('#otp_details_' + company_name).show();
                                        $('#otp_details_' + company_name).html(res[1]);
                                        if (res[2] != '') {
                                            
                                            $('#show_error_image_' + company_name).html("<a href='#' class='show_error_image width-40' error_img='"+res[2]+"'><image src='"+base_url+"/assets/icons/image_error_icon.png'></a>")
                                            $('#show_error_image_' + company_name).show();
                                        }
                                        $('#btn_' + company_name).removeAttr('disabled');

                                    }
                                }
                            })
                        }

                    })

                } else {
                    $('#' + company_name + '_loader').hide();
                    $('#otp_details_' + company_name).show();
                    $('#otp_details_' + company_name).html(res[1]);
                    if (res[2] != '') {
                        $('#show_error_image_' + company_name).html("<a href='#' class='show_error_image width-40' error_img='"+res[2]+"'><image src='"+base_url+"/assets/icons/image_error_icon.png'></a>")
                        $('#show_error_image_' + company_name).show();
                    }
                    $('#btn_' + company_name).removeAttr('disabled');
                }
                $('.resend_otp').on('click', function () {
                    $('#btn_' + $(this).attr('data-company-name')).removeAttr('disabled');
                    $('#btn_' + $(this).attr('data-company-name')).trigger('click');
                })
            }

        });
    }

})

$('#download_link').on('click', function () {
    $('#download_popup').modal('hide');
    location.reload();
})
$('#policy_details').validate({ // initialize the plugin
    rules: {
        social_id: {
            required: true,

        },
        mobile_number: {
            required: true,

        },
    },
    messages: {
        social_id: i18n.lan_constant.social_id_required,
        mobile_number: i18n.lan_constant.mobile_number_required,

    },
    submitHandler: function (form) {
        // do other things for a valid form
        if (ValidateID($('#social_id').val())) {
            form.submit();
        }

    }


});
$('#show_date').change(function () {
    if ($(this).is(":checked")) {
        $('#show_date_fields').show();
        $("#day").rules("add", {
            required: true,
            messages: {
                required: i18n.lan_constant.day_required,

            }
        });
        $("#month").rules("add", {
            required: true,
            messages: {
                required: i18n.lan_constant.month_required,

            }
        });
        $("#year").rules("add", {
            required: true,
            messages: {
                required: i18n.lan_constant.year_required,

            }
        });
    } else {
        $('#show_date_fields').hide();
        $("#month").rules("remove");
        $("#year").rules("remove");
        $("#day").rules("remove");
        $("#month").val("");
        $("#year").val("");
        $("#day").val("");
    }

});
var i = 0;
$('#all_companies').on('click', function () {
    var ins_companies = [
        'migdal',
        'clalbit',
        'menora',
        'phoenix',
        'harel',
        'ayalon',
        'haschara',
        'yashir',
        'nine_million',
        'aig',
    ];
    setInterval(function () {
        if (i < ins_companies.length) {
            $('#btn_' + ins_companies[i]).click();
        }
        i++;
    }, 1000);


})
$('#insurance_mountain_button').on('click', function () {
    if ($('#day').val() == '' || $('#month').val() == '' || $('#year').val() == '') {
        //Swal.fire(i18n.lan_constant.date_require_insurance_mountain);
        $('#myLargeModalLabel').html(i18n.lan_constant.date_require_insurance_mountain);
        $('#fill_date_modal').modal();


        return false;
    }

    $('#btn_' + $(this).attr('company_name')).click();
})

function ValidateID(str) {
    //INPUT VALIDATION

    // Just in case -> convert to string
    var IDnum = str.toString()

    // Validate correct input
    if ((IDnum.length > 9) || (IDnum.length < 5)) {
        $('.error_social_id').html(i18n.lan_constant.invalid_social_id)
        return false;
    }
    if (isNaN(str)) {
        $('.error_social_id').html(i18n.lan_constant.invalid_social_id)
        return false;
    }

    // The number is too short - add leading 0000
    if (IDnum.length < 9) {
        while (IDnum.length < 9) {
            IDnum = '0' + IDnum;
        }
    }

    // CHECK THE ID NUMBER
    var mone = 0,
        incNum;
    for (var i = 0; i < 9; i++) {
        incNum = Number(IDnum.charAt(i));
        incNum *= (i % 2) + 1;
        if (incNum > 9)
            incNum -= 9;
        mone += incNum;
    }
    if (mone % 10 == 0) {
        $('.error_social_id').html('')
        return true;
    } else {
        $('.error_social_id').html(i18n.lan_constant.invalid_social_id)
        return false;
    }
}
var modal = document.getElementById("myModal");

// Get the image and insert it inside the modal - use its "alt" text as a caption
$(document).on('click','.show_error_image',function () {
    var modalImg = document.getElementById("img01");
    var captionText = document.getElementById("caption");
    var modal = document.getElementById("myModal");
    modal.style.display = "block";
    modalImg.src = $(this).attr('error_img');
    //captionText.innerHTML = this.alt;

})


// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function () {
    modal.style.display = "none";
}
$("#body").keypress(function (e) {
    if (e.which == 13 && !$(e.target).is("textarea")) {
        return false;
    }
});
$('#fill_date').validate({ // initialize the plugin
    ignore: [],
    rules: {
        day_new: {
            required: true,

        },
        year_new: {
            required: true,

        },
        month_new: {
            required: true,

        },
    },
    messages: {
        day_new: i18n.lan_constant.day_required,
        year_new: i18n.lan_constant.year_required,
        month_new: i18n.lan_constant.month_required

    },
});
$(document).on("click", "#store_date", function (event) {
    if ($('#fill_date').valid()) {
        $('#day').val($('#day_new').val());
        $('#month').val($('#month_new').val());
        $('#year').val($('#year_new').val());
        $('#change_date').html($('#day_new').val() + '-' + $('#month_new').val() + '-' + $('#year_new').val());
        $('#fill_date_modal').modal('toggle');
    }

});
$(document).on("click",".bug_icon",function(event){
    $('#description').val('');
    $('#company_name').val($(this).attr('company_name'));
    $('#send_mail').modal('toggle');
    
})
$(document).on("click","#send_mail_button",function(event){
    $('.loader__label').html('Sending Mail...');
    $('.preloader').show();
    $.ajax({
        url: base_url + "/send_mail",
        method: 'post',
        data: {
            company_name:$('#company_name').val(),
            social_id: $('#social_id').val(),
            description : $('#description').val(),
            error_image : $('#show_error_image_'+$('#company_name').val()).attr('error_img')
        },
        success: function (result) {
            $('.preloader').hide();
           $('#send_mail').modal('toggle');
           Swal.fire("Mail sent Successfully")

        }
    })
})