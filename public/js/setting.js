if($('#language_selection').val() == 'hb'){
    $('#add_margin').removeClass('mr-4');
    $('#add_margin').addClass('ml-2');
    $('#remove_padding').addClass('pr-0');
}else{
    $('#add_margin').addClass('mr-4');
    $('#add_margin').removeClass('ml-2');
    $('#remove_padding').removeClass('pr-0');
}
$(function () {
   
    $('#store_setting').validate({ 
        rules: {
            logoUpload: {
                extension: "png|jpg|jpeg"
            },
            title_name:{required: true},
            callback_return_url:{
                required:true
            }
        },
        messages: {
            logoUpload: {
                        extension:i18n.lan_constant.upload_only_image
            },
            title_name:{required: i18n.lan_constant.enter_title},
            callback_return_url:{
                required: i18n.lan_constant.required_callback_return_url
            }
        },
    });
    
})


