$(function () {
    "use strict";
    $('#user_list').DataTable({
        responsive: true,
        "columns": [
            null,
            null,
            null,
            null,
            null,
            null,
            {
                "orderable": false
            }
        ]
    });
    $('#add_user_button').click(function () {
        location.href = base_url + '/users/add_user';
    })
    jQuery.validator.addMethod("valid_email", function (value, element) {
        return this.optional(element) || /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value);
    }, 'Please enter a valid email address.');
    $('#store_user_data').validate({ // initialize the plugin
        rules: {
            first_name: {
                required: true,

            },
            last_name: {
                required: true,

            },
            email: {
                required: true,
                valid_email: true

            },
            password: {
                required: true,

            },
            confirm_password: {
                required: true,
                equalTo: "#password"

            },
            role: {
                required: true,
            }
        },
        messages: {
            first_name: "Please enter your firstname",
            last_name: "Please enter your lastname",
            password: {
                required: "Please provide a password",
            },
            confirm_password: {
                required: "Please provide a confirm password",
                equalTo: "Please enter same password as password",
            },
            email: "Please enter a valid email address",
            role: "Please select role"
        },

    });
    $("#phone_number").on("keypress keyup blur", function (e) {
        var regex = new RegExp(/^(\?\+?[0-9]\?)?[0-9_\-+ ]*$/);
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);

        if (regex.test(str)) {
            return true;
        } else {
            e.preventDefault();
            return false;
        }

    });
    $('#role').trigger('change');
})
$('#role').on('change', function () {

        if ($(this).val() == 3 || $(this).val() == '3' || $(this).val() == 4 || $(this).val() == '4') {
            $('#show_hide_company').show();
            $("#company_id").rules("add", {
                required: true,
                messages: {
                    required: "Please Select Company.",

                }
            });
            $('#show_hide_details_for_company').hide();
            $('#s3_bucket_name').val();
            $('#s3_access_key').val();
            $('#s3_secret_key').val();
            $('#ftp_host').val();
            $('#ftp_password').val();
            $('#ftp_port').val();
            $('#ftp_username').val();
            $('#ftp_public_url').val();
            $('#webhook_url').val();
        } else {
            if ($(this).val() == 2 || $(this).val() == '2') {
                $('#show_hide_details_for_company').show();
            } else {
                $('#show_hide_details_for_company').hide();
                $('#s3_bucket_name').val();
                $('#s3_access_key').val();
                $('#s3_secret_key').val();
                $('#ftp_host').val();
                $('#ftp_password').val();
                $('#ftp_port').val();
                $('#ftp_username').val();
                $('#ftp_public_url').val();
                $('#webhook_url').val();
            }

            $('#show_hide_company').hide();
            $("#company_id").rules("remove");
        }
    })

    ! function ($) {
        "use strict";

        var SweetAlert = function () {};

        //examples 
        SweetAlert.prototype.init = function () {
                $(".sa-confirm").click(function () {
                    Swal.fire({
                        title: 'Are you sure?',
                        text: "You want to delete!",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, delete it!'
                    }).then((result) => {
                        if (result.value) {
                            $.ajax({
                                url: base_url + "/user/delete",
                                method: 'delete',
                                data: {
                                    user_id: $(this).attr('user_id')
                                },
                                success: function (result) {
                                    Swal.fire(
                                        'Deleted!',
                                        'User has been deleted.',
                                        'success'
                                    ).then((result) => {
                                        location.reload();
                                    })
                                }
                            });
                        }


                    })
                });

            },
            //init
            $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
    }(window.jQuery),

    //initializing 
    function ($) {
        "use strict";
        $.SweetAlert.init()
    }(window.jQuery);

function CopyToClipboard(val) {
    var hiddenClipboard = $('#_hiddenClipboard_');
    if (!hiddenClipboard.length) {
        $('body').append('<textarea style="position:absolute;top: -9999px;" id="_hiddenClipboard_"></textarea>');
        hiddenClipboard = $('#_hiddenClipboard_');
    }
    hiddenClipboard.html(val);
    hiddenClipboard.select();
    document.execCommand('copy');
    document.getSelection().removeAllRanges();
    hiddenClipboard.remove();
}
$('.copy_url').on('click', function () {
    CopyToClipboard($(this).data('clipboard-text'));
    Swal.fire(i18n.lan_constant.copy_link)
    
})

$('#ip_address').tagsinput({
    confirmKeys: [13, 188]
});


$('.bootstrap-tagsinput input').on('keypress', function (e) {
    if (e.keyCode == 13) {
        e.keyCode = 188;
        e.preventDefault();
    };
});
