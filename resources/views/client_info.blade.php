@extends('layout.mainlayout')

@section('content')
<div class="page-wrapper">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6 mt-3">
                <h3 class="text-center font-weight-bold mb-4 mt-5">{{ trans('lan_constant.new_customer_testing')}}</h3>
                <div class="card">
                    <div class="card-body">
                        <div class="row justify-content-center">
                            <div class="col-md-8">
                                <form id="policy_details" name="policy_details" method="get"
                                    action="{{url('/show_companies')}}">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="first_name" name="first_name"
                                            aria-describedby="first_name"
                                            placeholder="{{ trans('lan_constant.full_name')}}">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="social_id" name="social_id"
                                            placeholder="{{ trans('lan_constant.id')}}">
                                        <span class="error_social_id error"></span>
                                    </div>
                                    <div class="form-row form-group">
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="mobile_number"
                                                name="mobile_number"
                                                placeholder="{{ trans('lan_constant.mobile_phone')}}">
                                        </div>
                                        <div class="col-md-4">
                                            <select class="custom-select col-12" id="m_code" name="m_code">
                                                <option value="050">050</option>
                                                <option value="051">051</option>
                                                <option value="052">052</option>
                                                <option value="053">053</option>
                                                <option value="054">054</option>
                                                <option value="055">055</option>
                                                <option value="056">056</option>
                                                <option value="058">058</option>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="custom-control custom-checkbox mr-sm-2 mb-3">
                                        <input type="checkbox" class="custom-control-input" id="show_date" value="1"
                                            name="date">
                                        <label class="custom-control-label"
                                            for="show_date">{{ trans('lan_constant.add_an_insurance_money_mount_check')}}</label>

                                    </div>
                                    <div class="form-row form-group" style="display: none" id="show_date_fields">
                                        <div class="col-md-4">
                                            <select class="custom-select col-12" id="year" name="year">
                                                <option value="">{{ trans('lan_constant.a_year')}}</option>
                                                @for ($a=2019;$a>=1919;$a--)
                                                <option value="{{$a}}">{{$a}}</option>
                                                @endfor
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <select class="custom-select col-12" id="month" name="month">
                                                <option value="">{{ trans('lan_constant.a_month')}}</option>
                                                @for($a=1;$a<=12;$a++) 
                                                <option value="{{$a}}">{{$a}}</option>
                                                    @endfor
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <select class="custom-select col-12" id="day" name="day">
                                                <option value="">{{ trans('lan_constant.a_day')}}</option>
                                                @for($a=1;$a<=31;$a++) 
                                                <option value="{{$a}}">{{$a}}</option>
                                                    @endfor
                                            </select>
                                        </div>
                                    </div>
                                    <button type="reset" class="btn btn-danger" id="reset" name="reset"><i
                                            class="fas fa-times"></i> {{ trans('lan_constant.delete')}}</button>

                                    <button type="submit" class="btn btn-info px-5" name="submit" id="submit"><i
                                            class="fas fa-check"></i> {{ trans('lan_constant.continued')}} </button>

                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('pagescript')
<script src="{{url('/js/jquery_validation/jquery.validate.js')}}"></script>
<script src="{{url('/js/jquery_validation/additional-methods.js')}}"></script>
<script src="{{url('/js/lang.js')}}"></script>
<script src="{{asset('js/new_crawler.js')}}"></script>
@endsection