@if(!empty($response) && $response->status == 1)
<form id="verify_otp_form_{{$company_name}}" name="verify_otp_form_{{$company_name}}" method="post" data-form-validate="true">
    <input type="hidden" name="company_name_{{$company_name}}" id="company_name_{{$company_name}}" value="{{$company_name}}">
    <div class="row">
        <div class="col-md-4">
            <label for="message-text" class="control-label"
                style="margin-top: 8px;">{{ trans('lan_constant.enter_otp')}}:</label>
        </div>
        <div class="col-md-4">
            <input type="text" class="form-control required_field" id="otp_{{$company_name}}"
                name="otp_{{$company_name}}" data-rule-required="true" data-msg-required="Please enter otp">
            <input type="hidden" class="form-control" id="uuid_{{$company_name}}" name="uuid_{{$company_name}}"
                value="{{$response->data->user_uuid}}">
        </div>
        <div class="col-md-4">
            <button type="button" class="btn btn-info" id="submit_data_{{$company_name}}"
                name="submit_data_{{$company_name}}" data-company-name = "{{$company_name}}"><i class="ti-arrow-circle-right"></i></i></button>
                <button type="button" class="btn btn-info resend_otp" id="otp_resend_{{$company_name}}"
                name="otp_resend_{{$company_name}}" data-company-name = "{{$company_name}}"><i class="ti-back-right"></i></i></button>
        </div>
    </div>
</form>
@endif