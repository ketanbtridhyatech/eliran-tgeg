@if(!empty($response) && $response->status == 1)
<form id="verify_otp_form" name="verify_otp_form" method="post">
    <input type="hidden" name="company_name" id="company_name" value="{{$company_name}}">
    <h4 class="font-weight-bold text-capitalize">{{$company_name.":"}}</h4>
    <p>Otp sent successfully in {{$mobile}}</p> 
    <div class="form-row align-items-center">
        <div class="col-2">
            <label for="message-text" class="control-label mb-0">Enter OTP:</label>
        </div>
        <div class="col">
            <input type="text" class="form-control" id="otp_{{$company_name}}" name="otp_{{$company_name}}">
        </div>
        <div class="col">
            <button type="button" class="btn waves-effect waves-light btn-info otp_insurance_mountain" company_name = "{{$company_name}}"
                name="otp_insurance_mountain">Continue Process</button>
        </div>
    </div>
    <hr> 
</form>
@else
<form id="verify_otp_form" name="verify_otp_form" method="post">
    <input type="hidden" name="company_name" id="company_name" value="{{$company_name}}">
    <h4 class="font-weight-bold text-capitalize">{{$company_name.":"}}</h4>
    <p>{{$response->message}}</p>
    <hr> 
</form>
@endif