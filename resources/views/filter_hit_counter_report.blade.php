<thead>
    <tr>
        <th>#</th>
        <th>{{ trans('lan_constant.user_id')}}</th>
        <th>{{ trans('lan_constant.user_name')}}</th>
        <th>{{ trans('lan_constant.company_name')}}</th>
        <th>{{ trans('lan_constant.total_hits')}}</th>
        <th>{{ trans('lan_constant.total_success_hits')}}</th>
    </tr>
</thead>
<tbody>
    @if(!empty($users))
    <?php $i = 1; ?>
    @foreach($users as $user)
    <tr>
        <td>{{$i}}</td>
        <td>{{$user['id']}}</td>
        <td>{{$user['first_name']." ".$user['last_name']}}</td>
        <td>{{$user['company_name']}}</td>
        <td>{{$user['total_hits']}}</td>
        <td>{{$user['success_hit']}}</td>
        <?php
        $i++;
        ?>
    </tr>
    @endforeach
    @endif
</tbody>