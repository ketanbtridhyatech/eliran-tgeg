<thead>
    <tr>
        <th>#</th>
        <th>{{ trans('lan_constant.date_time')}}</th>
        <th>{{ trans('lan_constant.user_id')}}</th>
        <th>{{ trans('lan_constant.social_id')}}</th>
        <th>{{ trans('lan_constant.uuid')}}</th>
        <th>{{ trans('lan_constant.status')}}</th>
        <th>{{ trans('lan_constant.action')}}</th>
    </tr>
</thead>
<tbody>
    @if(!empty($inv_log))
    <?php $i=1; ?>
    @foreach($inv_log as $log)
    <tr>
        <td>{{$i}}</td>
        <td>{{$log['datetime']}}</td>
        <td>{{$log['user_id']}}</td>
        <td>{{$log['social_id']}}</td>
        <td>{{$log['user_uuid']}}</td>
        <td>{{$log['crawler_result']==1?'Success':'No Files Back'}}</td>
        <td><a href="#" id="{{$log['id']}}" class="show_log"><i
                    class="fa fas fa-file"></i></a></td>
        <?php
        $i++;
        ?>
    </tr>
    @endforeach
    @endif
</tbody>