@extends('layout.mainlayout')

@section('content')
<!-- ============================================================== -->
<!-- End Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">{{ trans('lan_constant.user_hits_report')}}</h4>
            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">{{ trans('lan_constant.home')}}</a>
                        </li>
                        <li class="breadcrumb-item active">{{ trans('lan_constant.user_hits_report')}}</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="form-group col-md-4">
                                <label for="role">{{trans('lan_constant.date_range')}}</label>
                                <input type="text" class="form-control" id="date_range" name="date_range"
                                    placeholder="{{trans('lan_constant.date_range')}}">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="role">{{trans('lan_constant.company')}} </label>
                                <select id="company_name" name="company_name" class="form-control">
                                    <option value="">{{trans('lan_constant.select_company')}}</option>
                                    @if(!empty($companies))
                                    @foreach($companies as $company)
                                    <option value="{{$company['id']}}">{{$company['first_name']." ".$company['last_name']}}</option>
                                    @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="role">{{trans('lan_constant.user_name')}}</label>
                                <select id="user_name" name="user_name" class="form-control">
                                    <option value="">{{trans('lan_constant.select_user')}}</option>
                                    @if(!empty($users))
                                    @foreach($users as $user)
                                    <option value="{{$user['id']}}">{{$user['first_name']." ".$user['last_name']}}</option>
                                    @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="table-responsive m-t-40">
                            <table id="user_list" class="table display table-bordered table-striped no-wrap">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{ trans('lan_constant.user_id')}}</th>
                                        <th>{{ trans('lan_constant.user_name')}}</th>
                                        <th>{{ trans('lan_constant.company')}}</th>
                                        <th>{{ trans('lan_constant.total_hits')}}</th>
                                        <th>{{ trans('lan_constant.total_success_hits')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!empty($users))
                                    <?php $i=1; ?>
                                    @foreach($users as $user)
                                    <tr>
                                        <td>{{$i}}</td>
                                        <td>{{$user['id']}}</td>
                                        <td>{{$user['first_name']." ".$user['last_name']}}</td>
                                        <td>{{$user['company_name']}}</td>
                                        <td>{{$user['total_hits']}}</td>
                                        <td>{{$user['success_hit']}}</td>
                                        <?php
                                        $i++;
                                        ?>
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('pagecss')
<link rel="stylesheet" type="text/css"
    href="{{url('/assets/node_modules/datatables.net-bs4/css/dataTables.bootstrap4.css')}}">
<link rel="stylesheet" type="text/css"
    href="{{url('/assets/node_modules/datatables.net-bs4/css/responsive.dataTables.min.css')}}">
<link href="{{url('/assets/node_modules/bootstrap-daterangepicker/daterangepicker.css')}}" rel="stylesheet">
@endsection
@section('pagescript')
<script src="{{url('/js/jquery_validation/jquery.validate.js')}}"></script>
<script src="{{url('/js/jquery_validation/additional-methods.js')}}"></script>
<script src="{{url('/assets/node_modules/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{url('/assets/node_modules/datatables.net-bs4/js/dataTables.responsive.min.js')}}"></script>
<script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
<script src="{{url('assets/node_modules/moment/moment.js')}}"></script>
<script src="{{url('/assets/node_modules/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<script src="{{url('/js/hit_counter_report.js?'.date('Ymdhis'))}}"></script>
<script src="{{url('/js/lang.js?'.date('dmYhis'))}}"></script>
<script>
    $(function () {
        @if(app()->getLocale() == 'en' && !empty(Session::has('success')))      
        $.toast({
            heading: 'Success',
            text: "{{Session::get('success')}}"
            , position: 'top-right'
            , loaderBg: '#ff6849'
            , icon: 'success'
            , hideAfter: 3500
            , stack: 6
        })
        @elseif(!empty(Session::has('success')))
        $.toast({
            heading: 'Success',
            text: "{{Session::get('success')}}"
            , position: 'top-left'
            , loaderBg: '#ff6849'
            , icon: 'success'
            , hideAfter: 3500
            , stack: 6
        })
        
        @endif
     
    })
</script>
@endsection