@php $totalLogs = 0;@endphp
@if(!$getTotalLogs->isEmpty())
@foreach($getTotalLogs as $val)
@php $totalLogs = $totalLogs + $val->total_logs; @endphp
@endforeach
@endif
<div class="card-body bg-light">
    <div class="row">
        <div class="col-6">
            <h3>{{$date}}</h3>
        </div>
        <div class="col-6 align-self-center display-6 text-right">
            <h2 class="text-success">{{$totalLogs}}</h2>
            <h5 class="font-light m-t-0">{{ trans('lan_constant.dashborad_social_id_checks')}}
            </h5>
        </div>
    </div>
</div>
<div class="table-responsive">
    <table class="table table-hover no-wrap">
        <thead>
            <tr>
                <th class="text-center">#</th>
                <th>{{ trans('lan_constant.dashborad_agent_name')}}</th>
                <th>{{ trans('lan_constant.dashborad_date')}}</th>
                <th>{{ trans('lan_constant.dashborad_no_of_social_id_hits')}}</th>
            </tr>
        </thead>
        <tbody>
            @php $i = 1;@endphp
            @if(!$getTotalLogs->isEmpty())
            @foreach($getTotalLogs as $val)
            <tr>
                <td class="text-center">{{$i}}</td>
                <td class="txt-oflo">{{$val->first_name.' '.$val->last_name.' '.$val->id}}</td>
                <td class="txt-oflo">{{Date('d-m-Y',strtotime($val->datetime))}}</td>
                <td><span class="text-success"> {{$val->total_logs}}</span></td>
            </tr>
            @php $i++;@endphp
            @endforeach
            @endif
        </tbody>
    </table>
</div>