@extends('layout.mainlayout')

@section('content')
<!-- ============================================================== -->
<!-- End Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">{{ trans('lan_constant.dashboard')}}</h4>
            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard 1</li>
                    </ol>
                    <!-- <button type="button" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Create New</button>-->
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Info box -->
        <!-- ============================================================== -->

        @if(Auth::user()->hasRole('Super admin') || Auth::user()->hasRole('Company'))
        <div class="card-group">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="d-flex no-block align-items-center">
                                <div>
                                    <h3><i class="icon-people"></i></h3>
                                    <p class="text-muted">{{ trans('lan_constant.users')}}</p>
                                </div>
                                <div class="ml-auto">
                                    <h2 class="counter text-primary">{{$users}}</h2>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="progress">
                                <div class="progress-bar bg-primary" role="progressbar" style="width: 85%; height: 6px;"
                                    aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <!-- Column -->
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title">{{ trans('lan_constant.dashborad_daily_api_calls')}}</h5>
                                    <div class="row">
                                        <div class="col-6  m-t-30">
                                            <h1 class="text-info hiteCount"></h1>
                                            <p class="text-muted"></p>
                                            <b></b>
                                        </div>
                                        <div class="col-6">
                                            <div id="sparkline2dash" class="text-right"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Column -->
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <!-- Column -->
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title">{{ trans('lan_constant.dashborad_successfull_api_result')}}
                                    </h5>
                                    <div class="row">
                                        <div class="col-6  m-t-30">
                                            <h1 class="text-info successHitTotal"></h1>
                                        </div>
                                        <div class="col-6">
                                            <div id="sales1" class="text-right"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Column -->
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex">
                            <div>
                                <h5 class="card-title">{{ trans('lan_constant.dashborad_social_id_report')}}</h5>
                                <h6 class="card-subtitle"></h6>
                            </div>
                            <div class="ml-auto">
                                <select class="form-control b-0" id="social_id_report_type">
                                    <option value="today">{{ trans('lan_constant.dashborad_today')}}</option>
                                    <option value="yesterday">{{ trans('lan_constant.dashborad_yesterday')}}</option>
                                    <option value="this_week">{{ trans('lan_constant.dashborad_this_week')}}</option>
                                    <option value="this_month">{{ trans('lan_constant.dashborad_this_month')}}</option>
                                    <option value="last_week">{{ trans('lan_constant.dashborad_last_week')}}</option>

                                </select>
                            </div>
                        </div>
                    </div>
                    <div id="response_data">
                     
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
</div>
</div>

@endif

@if(Auth::user()->hasRole('Agent'))
<div class="row">

    <div class="col-md-2">
        <div class="card">
            <a href="javascript:" class="get_info" company_name="migdal">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="d-flex no-block align-items-center">
                                <h3 class="text-center w-100">Migdal</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>

    <div class="col-md-2">
        <div class="card">
            <a href="javascript:" class="get_info" company_name="clalbit">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="d-flex no-block align-items-center">
                                <h3 class="text-center w-100">Clal</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>

    <div class="col-md-2">
        <div class="card">
            <a href="javascript:" class="get_info" company_name="menora">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="d-flex no-block align-items-center">
                                <h3 class="text-center w-100">Menora</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>

    </div>


    <div class="col-md-2">
        <div class="card">
            <a href="javascript:" class="get_info" company_name="phoenix">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="d-flex no-block align-items-center">
                                <h3 class="text-center w-100">Phoenix</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>


    <div class="col-md-2">
        <div class="card">
            <a href="javascript:" class="get_info" company_name="harel">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="d-flex no-block align-items-center">
                                <h3 class="text-center w-100">Harel</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>


    <div class="col-md-2">
        <div class="card">
            <a href="javascript:" class="get_info" company_name="ayalon">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="d-flex no-block align-items-center">
                                <h3 class="text-center w-100">Ayalon</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>


    <div class="col-md-2">
        <div class="card">
            <a href="javascript:" class="get_info" company_name="hachshara">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="d-flex no-block align-items-center">
                                <h3 class="text-center w-100">Hachshara</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>


    <div class="col-md-2">
        <div class="card">
            <a href="javascript:" class="get_info" company_name="bituach yashir">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="d-flex no-block align-items-center">
                                <h3 class="text-center w-100">Bituach Yashir</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>


    <div class="col-md-2">
        <div class="card">
            <a href="javascript:" class="get_info" company_name="money mountain">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="d-flex no-block align-items-center">
                                <h3 class="text-center w-100">Money Mountain</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>

    <div class="col-md-2">
        <div class="card">
            <a href="javascript:" class="get_info" company_name="insurance mountain">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="d-flex no-block align-items-center">
                                <h3 class="text-center w-100">Mount of Insurance</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>


    <div class="col-md-2">
        <div class="card">
            <a href="javascript:" class="get_info" company_name="nine_million">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="d-flex no-block align-items-center">
                                <h3 class="text-center w-100">9,000,000</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>


    <div class="col-md-2">
        <div class="card">
            <a href="javascript:" class="get_info" company_name="aig">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="d-flex no-block align-items-center">
                                <h3 class="text-center w-100">AIG</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>

</div>

@endif
<!-- Column -->
<!-- Column -->


<!-- ============================================================== -->
<!-- End Info box -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Over Visitor, Our income , slaes different and  sales prediction -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- End Page Content -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Todo, chat, notification -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- End Page Content -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Right sidebar -->
<!-- ============================================================== -->
<!-- .right-sidebar -->

<!-- ============================================================== -->
<!-- End Right sidebar -->
<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
</div>
<div id="get_policy_data_modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header flex-row-reverse">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="fill_client_details">Fill client details</h4>
                <h4 class="modal-title" id="enter_otp">Enter Otp</h4>
            </div>
            <div class="modal-body" id="modal_body">
                <form id="policy_details" name="policy_details">
                    <input type="hidden" class="form-control" id="company_name" name="company_name">
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">SocialId: <span class="error">*</span></label>
                        <input type="text" class="form-control" id="social_id" name="social_id">
                    </div>
                    <div class="form-group" id="mobile_number_data">
                        <label for="message-text" class="control-label">Mobile: <span class="error">*</span></label>
                        <input type="text" class="form-control" id="mobile_number" name="mobile_number">
                    </div>
                    <div class="form-group" id="socialid_issue_date_data">
                        <label for="message-text" class="control-label">Socialid issue date: <span
                                class="error show_astrict" style="display:none">*</span></label>
                        <input type="text" class="form-control" id="social_id_issue_date" name="social_id_issue_date">
                    </div>
                    <div class="custom-control custom-checkbox mr-sm-2 mb-3 force_login" style="display:none">
                        <input type="checkbox" class="custom-control-input" id="force_login" name="force_login"
                            value="true">
                        <label class="custom-control-label" for="force_login">Force Login</label>
                    </div>
                    <div id="otp_message" name="otp_message"></div>
                    <div class="captcha" id="captcha">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary waves-effect" data-dismiss="modal">Close</button>
                <button type="button" class="btn waves-effect waves-light btn-info" id="submit_data"
                    name="submit_data">Continue Process</button>
                <button type="button" class="btn waves-effect waves-light btn-info" id="get_captcha"
                    name="get_captcha">Start Process</button>
            </div>
        </div>
    </div>
</div>
<div id="download_popup" class="modal" tabindex="-1" role="dialog" aria-labelledby="download_popup" aria-hidden="true"
    style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header flex-row-reverse">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Downloads</h4>
            </div>

            <div class="modal-body" id="modal_body">
                <div class="form-group">
                    <label for="message-text" class="control-label">Download Your files:</label>
                    <a class="btn waves-effect waves-light btn-info form-control" id="download_link"
                        name="download_link" href="#" download target="_blank">Download</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="otp_modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="otp_modal" aria-hidden="true"
    style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header flex-row-reverse">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">OTP</h4>
            </div>

            <div class="modal-body" id="modal_body_otp">

            </div>
        </div>
    </div>
</div>
@endsection
@section('pagecss')
<link href="{{url('/assets/node_modules/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet">
@endsection
@section('pagescript')
<script src="{{url('/js/jquery_validation/jquery.validate.js')}}"></script>
<script src="{{url('/js/jquery_validation/additional-methods.js')}}"></script>
<script src="{{url('/assets/node_modules/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('js/dashboard1.js')}}"></script>
@endsection