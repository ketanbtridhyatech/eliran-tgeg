@extends('layout.mainlayout')

@section('content')
<!-- ============================================================== -->
<!-- End Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">{{ trans('lan_constant.investigations_log')}}</h4>
            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">{{ trans('lan_constant.home')}}</a>
                        </li>
                        <li class="breadcrumb-item active">{{ trans('lan_constant.investigations_logs')}}</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="form-group col-md-4">
                                <label for="role">{{trans('lan_constant.social_id')}}</label>
                                <input type="text" class="form-control text" id="social_id" name="social_id"
                                    placeholder="{{trans('lan_constant.social_id')}}">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="role">{{trans('lan_constant.uuid')}}</label>
                                <input type="text" class="form-control text" id="uuid" name="uuid"
                                    placeholder="{{trans('lan_constant.uuid')}}">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="role">{{trans('lan_constant.date')}}</label>
                                <input type="text" class="form-control" id="date" name="date"
                                    placeholder="{{trans('lan_constant.date')}}">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="role">{{trans('lan_constant.status')}}</label>
                                <select id="status" name="status" class="form-control">
                                    <option value="">{{trans('lan_constant.status')}}</option>
                                    <option value="1">{{trans('lan_constant.success')}}</option>
                                    <option value="0">{{trans('lan_constant.no_files_back')}}</option>
                                </select>
                            </div>
                        </div>
                        <div class="table-responsive m-t-40">
                            <table id="log_list" class="table display table-bordered table-striped no-wrap">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{ trans('lan_constant.date_time')}}</th>
                                        <th>{{ trans('lan_constant.user_id')}}</th>
                                        <th>{{ trans('lan_constant.social_id')}}</th>
                                        <th>{{ trans('lan_constant.uuid')}}</th>
                                        <th>{{ trans('lan_constant.status')}}</th>
                                        <th>{{ trans('lan_constant.action')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!empty($inv_log))
                                    <?php $i=1; ?>
                                    @foreach($inv_log as $log)
                                    <tr>
                                        <td>{{$i}}</td>
                                        <td>{{$log['datetime']}}</td>
                                        <td>{{$log['user_id']}}</td>
                                        <td>{{$log['social_id']}}</td>
                                        <td>{{$log['user_uuid']}}</td>
                                        <td>{{$log['crawler_result']==1?'Success':'No Files Back'}}</td>
                                        <td><a href="javascript:" id="{{$log['id']}}" class="show_log"><i
                                                    class="fa fas fa-file"></i></a></td>
                                        <?php
                                        $i++;
                                        ?>
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal" tabindex="-1" role="dialog" id="json_modal">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Json File</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <pre id="json_data"></pre>
        </div>
        <div class="modal-footer">
          
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
@endsection
@section('pagecss')
<link rel="stylesheet" type="text/css"
    href="{{url('/assets/node_modules/datatables.net-bs4/css/dataTables.bootstrap4.css')}}">
<link rel="stylesheet" type="text/css"
    href="{{url('/assets/node_modules/datatables.net-bs4/css/responsive.dataTables.min.css')}}">
<link href="{{url('/assets/node_modules/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet">
@endsection
@section('pagescript')
<script src="{{url('/js/jquery_validation/jquery.validate.js')}}"></script>
<script src="{{url('/js/jquery_validation/additional-methods.js')}}"></script>
<script src="{{url('/assets/node_modules/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{url('/assets/node_modules/datatables.net-bs4/js/dataTables.responsive.min.js')}}"></script>
<script src="{{url('assets/node_modules/moment/moment.js')}}"></script>
<script src="{{url('/assets/node_modules/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
<script src="{{url('/js/investigations_log.js?'.date('Ymdhis'))}}"></script>
<script src="{{url('/js/lang.js?'.date('dmYhis'))}}"></script>
<script>
    $(function () {
        @if(app()->getLocale() == 'en' && !empty(Session::has('success')))      
        $.toast({
            heading: 'Success',
            text: "{{Session::get('success')}}"
            , position: 'top-right'
            , loaderBg: '#ff6849'
            , icon: 'success'
            , hideAfter: 3500
            , stack: 6
        })
        @elseif(!empty(Session::has('success')))
        $.toast({
            heading: 'Success',
            text: "{{Session::get('success')}}"
            , position: 'top-left'
            , loaderBg: '#ff6849'
            , icon: 'success'
            , hideAfter: 3500
            , stack: 6
        })
        
        @endif
     
    })
</script>
@endsection