<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="{{app()->getLocale()}}">

<head>
    @include('layout.partials.head')
</head>

<body class="skin-blue fixed-layout" id="body">
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Please wait...</p>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        @include('layout.partials.navigation')
        @yield('content')
        @include('layout.partials.footer')
    </div>
    @include('layout.partials.footer-scripts')
    @yield('pagecss')
    @yield('pagescript')
</body>

</html>