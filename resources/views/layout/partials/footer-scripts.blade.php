<script src="{{asset('assets/node_modules/jquery/jquery-3.2.1.min.js')}}"></script>
<!-- Bootstrap popper Core JavaScript -->
<script src="{{asset('assets/node_modules/popper/popper.min.js')}}"></script>
<script src="{{asset('assets/node_modules/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="{{asset('js/perfect-scrollbar.jquery.min.js')}}"></script>
<!--Wave Effects -->
<script src="{{asset('js/waves.js')}}"></script>
<!--Menu sidebar -->
<script src="{{asset('js/sidebarmenu.js')}}"></script>
<!--Custom JavaScript -->
<script src="{{asset('js/custom.min.js')}}"></script>
<!-- ============================================================== -->
<!-- This page plugins -->
<!-- ============================================================== -->
<!--morris JavaScript -->
<script src="{{asset('assets/node_modules/raphael/raphael-min.js')}}"></script>
<!-- <script src="{{asset('assets/node_modules/morrisjs/morris.min.js')}}"></script>-->
<script src="{{asset('assets/node_modules/jquery-sparkline/jquery.sparkline.min.js')}}"></script>
<!-- Popup message jquery -->
<script src="{{asset('assets/node_modules/toast-master/js/jquery.toast.js')}}"></script>
<!-- Chart JS -->

<script src="{{asset('assets/node_modules/toast-master/js/jquery.toast.js')}}"></script>
<script src="{{asset('assets/node_modules/sweetalert2/dist/sweetalert2.all.min.js')}}"></script>
<script src="{{asset('assets/node_modules/sweetalert2/sweet-alert.init.js')}}"></script>
<script>
    var base_url = "{{url('')}}";    
</script>
<script>
    $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                  }
              });
    $('#language_selection').on('change',function(){
            
                $.ajax({
                        url: base_url+"/change_language", 
                        method:'post',
                        data:{
                            lang:$(this).val()
                        },
                        success: function(result){
                           location.reload();
                                }
                            });
           })
</script>