<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Tell the browser to be responsive to screen width -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<meta name="_token" content="{{ csrf_token() }}">
<metahttp-equiv='cache-control' content='no-cache'>
<metahttp-equiv='expires' content='0'>
<metahttp-equiv='pragma' content='no-cache'>
            <!-- Favicon icon -->
            <link rel="icon" type="image/png" sizes="16x16" href="{{asset('assets/images/favicon.png')}}">
            <title>{{(new \App\Helpers\CommonHelper)->getSettingData()['title']}}</title>
            <!-- This page CSS -->
            <!-- chartist CSS -->
            <link href="https://fonts.googleapis.com/css?family=Assistant:200,300,400,600,700,800&display=swap"
                rel="stylesheet">
            <link href="{{asset('assets/node_modules/morrisjs/morris.css')}}" rel="stylesheet">
            <!--Toaster Popup message CSS -->
            <link href="{{asset('assets/node_modules/toast-master/css/jquery.toast.css')}}" rel="stylesheet">
            <!-- Custom CSS -->
            <link href="{{asset('css/'.app()->getLocale().'_css/style.min.css?'.date('dmYhis'))}}" rel="stylesheet">
            <!-- Dashboard 1 Page CSS -->
            <link href="{{asset('css/'.app()->getLocale().'_css/dashboard1.css?'.date('dmYhis'))}}" rel="stylesheet">
            <link href="{{asset('css/custom.css')}}" rel="stylesheet">
            <link href="{{asset('css/'.app()->getLocale().'_css/custom.css?'.date('dmYhis'))}}" rel="stylesheet">
            <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
            <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
            <!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->