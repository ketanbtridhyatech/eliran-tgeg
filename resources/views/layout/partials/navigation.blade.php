
  <!-- ============================================================== -->
  <!-- Topbar header - style you can find in pages.scss -->
  <!-- ============================================================== -->
  <header class="topbar">
      <nav class="navbar top-navbar navbar-expand-md navbar-dark">
          <!-- ============================================================== -->
          <!-- Logo -->
          <!-- ============================================================== -->
          <div class="navbar-header">
              <a class="navbar-brand" href="{{url('/dashboard')}}">
                  <!-- Logo icon --><b>
                  </b>
                  <!--End Logo icon -->
                  <!-- Logo text --><span>
                   <img src="{{asset('assets/images/'.(new \App\Helpers\CommonHelper)->getSettingData()['site_logo'])}}" class="light-logo" alt="homepage" /></span> </a>
          </div>
          <!-- ============================================================== -->
          <!-- End Logo -->
          <!-- ============================================================== -->
          <div class="navbar-collapse">
              <!-- ============================================================== -->
              <!-- toggle and nav items -->
              <!-- ============================================================== -->
              <ul class="navbar-nav mr-auto">
                  <!-- This is  -->
                  <li class="nav-item"> <a class="nav-link nav-toggler d-block d-md-none waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                  <li class="nav-item"> <a class="nav-link sidebartoggler d-none d-lg-block d-md-block waves-effect waves-dark" href="javascript:void(0)"><i class="icon-menu"></i></a> </li>
                  <!-- ============================================================== -->
                  <!-- Search -->
                  <!-- ============================================================== -->
                  <li class="nav-item">
                      <div class="d-flex flex-column justify-content-center h-100">  
                        <select class="form-control custom-select" name="langugae_selection" id="language_selection">
                            <option value="en" {{app()->getLocale()=='en'?'selected':''}}>English</option>
                            <option value="hb" {{app()->getLocale()=='hb'?'selected':''}}>Hebrew</option>
                            </select>
                    </div>
                  </li>
               
              </ul>
              <!-- ============================================================== -->
              <!-- User profile and search -->
              <!-- ============================================================== -->
              <ul class="navbar-nav my-lg-0">
               
                  <!-- ============================================================== -->
                  <!-- End mega menu -->
                  <!-- ============================================================== -->
                  <!-- ============================================================== -->
                  <!-- User Profile -->
                  <!-- ============================================================== -->
                  <li class="nav-item dropdown u-pro">
                      <a class="nav-link dropdown-toggle waves-effect waves-dark profile-pic" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <span class="hidden-md-down">{{Auth::user()->first_name}}&nbsp;{{Auth::user()->last_name}} &nbsp;<i class="fa fa-angle-down"></i></span> </a>
                      <div class="dropdown-menu dropdown-menu-right animated flipInY">
                          <!-- text-->
                           <a href="javascript:void(0)" class="dropdown-item"><i class="ti-user"></i> My Profile</a>
                          <!-- text
                         <!--  <a href="javascript:void(0)" class="dropdown-item"><i class="ti-wallet"></i> My Balance</a>-->
                          <!-- text-->
                          <!-- <a href="javascript:void(0)" class="dropdown-item"><i class="ti-email"></i> Inbox</a> -->
                          <!-- text-->
                          <!-- <div class="dropdown-divider"></div> -->
                          <!-- text-->
                         <!--  <a href="javascript:void(0)" class="dropdown-item"><i class="ti-settings"></i> Account Setting</a>-->
                          <!-- text-->
                          <!-- <div class="dropdown-divider"></div>-->
                          <!-- text-->
                          <a href="{{route('logout')}}" class="dropdown-item"><i class="fa fa-power-off"></i> Logout</a>
                          <!-- text-->
                      </div>
                  </li>
                  <!-- ============================================================== -->
                  <!-- End User Profile -->
                  <!-- ============================================================== -->
                  <li class="nav-item right-side-toggle"> <a class="nav-link  waves-effect waves-light" href="javascript:void(0)"><i class="ti-settings"></i></a></li>
              </ul>
          </div>
      </nav>
  </header>
  <!-- ============================================================== -->
  <!-- End Topbar header -->
  <!-- ============================================================== -->
  <!-- ============================================================== -->
  <!-- Left Sidebar - style you can find in sidebar.scss  -->
  <!-- ============================================================== -->
  <aside class="left-sidebar">
      <!-- Sidebar scroll-->
      <div class="scroll-sidebar">
          <!-- Sidebar navigation-->
          <nav class="sidebar-nav">
              <ul id="sidebarnav">
          @if(Auth::user()->hasRole('Super admin') ||  Auth::user()->hasRole('Company') || Auth::user()->hasPermission('View super admin'))
                  <li> <a class="has-arrow waves-effect waves-dark {{ (request()->is('users/*'))? 'active' : '' }}" href="javascript:void(0)" aria-expanded="false"><i class="icon-people"></i><span class="hide-menu">{{ trans('lan_constant.manage_users')}} <span class="badge badge-pill badge-cyan ml-auto"></span></span></a>
                      <ul aria-expanded="false" class="collapse {{ (request()->is('users/edit/*') ) ? 'in' : '' }}">
                          <li class="{{ (request()->is('users/add_user') || request()->is('users/edit/*') ) ? 'active' : '' }}"><a href="{{url('/users/add_user')}}">{{ trans('lan_constant.add_user')}} </a></li>
                          <li><a href="{{url('/users')}}">{{ trans('lan_constant.users')}} </a></li>
                         <!-- <li><a href="#">{{ trans('lan_constant.users_activity_logs')}} </a></li>-->
                      </ul>
                    </li>
                @if(Auth::user()->hasRole('Super admin') || Auth::user()->hasPermission('View super admin'))
                  <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="icon-user"></i><span class="hide-menu">{{ trans('lan_constant.monitor')}} <span class="badge badge-pill badge-cyan ml-auto"></span></span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{url('/activity/logs')}}">{{ trans('lan_constant.crawlers_activity_log')}} </a></li>
                        <li><a href="{{url('/activity/investigations_log')}}">{{ trans('lan_constant.investigations_log')}} </a></li>
                    </ul>
            
                </li>
                
                @endif
                @if(Auth::user()->hasRole('Super admin') ||  Auth::user()->hasPermission('View super admin') || Auth::user()->hasPermission('View settings page'))
                <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="icon-settings"></i><span class="hide-menu">{{ trans('lan_constant.settings')}} <span class="badge badge-pill badge-cyan ml-auto"></span></span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{url('/general_setting_page')}}">{{ trans('lan_constant.general_setting_page')}} </a></li>
                        @if(Auth::user()->hasRole('Super admin'))
                        <!-- <li><a href="#">{{ trans('lan_constant.billing_settings')}} </a></li>-->
                        @endif
                        <li><a href="{{url('/permission_settings')}}">{{ trans('lan_constant.permission_settings')}} </a></li>
                    </ul>
                </li>
                @endif
                @if(Auth::user()->hasRole('Agent') ||  Auth::user()->hasPermission('View super admin') || Auth::user()->hasPermission('View billing page'))
                <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="icon-notebook"></i><span class="hide-menu">{{ trans('lan_constant.billing')}} <span class="badge badge-pill badge-cyan ml-auto"></span></span></a>
                    <ul aria-expanded="false" class="collapse">
                        <!-- <li><a href="#">{{ trans('lan_constant.all_users_hits_activity')}} </a></li>-->
                        <li><a href="{{url('billing/billing_report')}}">{{ trans('lan_constant.reports')}} </a></li>
                    </ul>
                </li>
                @endif
          @else
           <!-- <li> 
                <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                    <i class="icon-user"></i>
                    <span class="hide-menu">{{ trans('lan_constant.check_new_client')}} 
                        <span class="badge badge-pill badge-cyan ml-auto"></span>
                    </span>
                </a>
                <ul aria-expanded="false" class="collapse">
                    <li><a href="{{url('/new_client')}}">{{ trans('lan_constant.check_new_client')}} </a></li>
                </ul>
            </li>-->
            <li> <a class="waves-effect waves-dark active" href="{{url('/new_client')}}" aria-expanded="false"><i class="icon-user"></i><span class="hide-menu">{{ trans('lan_constant.check_new_client')}}</span></a></li>
           <!-- <li> 
                    <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                        <i class="icon-user"></i>
                        <span class="hide-menu">New Page
                            <span class="badge badge-pill badge-cyan ml-auto"></span>
                        </span>
                    </a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{url('/client_info_page')}}">New page</a></li>
                    </ul>
                </li>-->
          @endif
              </ul>
          </nav>
          <!-- End Sidebar navigation -->
      </div>
      <!-- End Sidebar scroll-->
  </aside>
  
  <!-- ============================================================== -->
  <!-- End Left Sidebar - style you can find in sidebar.scss  -->
  <!-- ============================================================== -->
  <!-- ============================================================== -->
  <!-- Page wrapper  -->
  <!-- ============================================================== -->