@extends('layout.mainlayout')

@section('content')
<!-- ============================================================== -->
<!-- End Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">{{ trans('lan_constant.log_list')}}</h4>
            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">{{ trans('lan_constant.home')}}</a></li>
                        <li class="breadcrumb-item active">{{ trans('lan_constant.logs')}}</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive m-t-40">
                            <table id="activity_log_list" class="table display table-bordered table-striped no-wrap">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{ trans('lan_constant.agentID')}}</th>
                                        <th>{{ trans('lan_constant.on_Id')}}</th>
                                        <th>{{ trans('lan_constant.action_desc')}}</th>
                                        <th>{{ trans('lan_constant.date_time')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!empty($logs))
                                    <?php $i=1; ?>
                                    @foreach($logs as $log)
                                    <tr>
                                        <td>{{$i}}</td>
                                        <td>{{$log->agent_id}}</td>
                                        <td>{{$log->on_id}}</td>
                                        <td>{{$log->action_desc}}</td>
                                        <td>{{$log->datetime}}</td>
                                        <?php
                                        $i++;
                                        ?>
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('pagecss')
<link rel="stylesheet" type="text/css" href="{{url('/assets/node_modules/datatables.net-bs4/css/dataTables.bootstrap4.css')}}">
<link rel="stylesheet" type="text/css" href="{{url('/assets/node_modules/datatables.net-bs4/css/responsive.dataTables.min.css')}}">
@endsection
@section('pagescript')
<script src="{{url('/assets/node_modules/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{url('/assets/node_modules/datatables.net-bs4/js/dataTables.responsive.min.js')}}"></script>
<script src="{{url('/js/log.js')}}"></script>
@endsection