@extends('layout.mainlayout')

@section('content')
<div class="page-wrapper new_client_wrap">
    <div class="container">

        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <!--<div class="card-header">New Client</div>-->
                    <div class="card-body">
                        <input type="hidden" name="day" id="day" value="{{$data['day']}}">
                        <input type="hidden" name="month" id="month" value="{{$data['month']}}">
                        <input type="hidden" name="year" id="year" value="{{$data['year']}}">
                        <input type="hidden" name="mobile_number" id="mobile_number"
                            value="{{$data['m_code'].$data['mobile_number']}}">
                        <input type="hidden" name="process_id" id="process_id" value="{{session('process_id')}}">
                        <input type="hidden" name="agent_id" id="agent_id"
                            value="{{isset($data['agent_id'])?$data['agent_id']:''}}">
                        <input type="hidden" name="social_id" id="social_id" value="{{$data['social_id']}}">
                        <input type="hidden" name="record_id" id="record_id"
                            value="{{isset($data['record_id'])?$data['record_id']:''}}">
                        <div class="info-panel">
                            <div class="panel panel-name">
                                <div class="white-panel">
                                    {{$data['first_name']}}
                                </div>
                                <div class="gray-panel">
                                    <h5> {{ trans('lan_constant.customer_under_review')}} </h5>
                                </div>
                            </div>
                            <div class="panel ">
                                <div class="white-panel" id="change_date">
                                    @if(!empty($data['year']))
                                    {{$data['day']."-".$data['month']."-".$data['year']}}

                                    @endif
                                </div>
                                <div class="gray-panel">
                                    <h5> {{ trans('lan_constant.issue_date')}}</h5>
                                </div>
                                <div class="white-panel">
                                    {{$data['m_code'].$data['mobile_number']}}
                                </div>
                                <div class="gray-panel">
                                    <h5> {{ trans('lan_constant.phone')}}</h5>
                                </div>
                                <div class="white-panel">
                                    {{$data['social_id']}}
                                </div>
                                <div class="gray-panel">
                                    <h5> {{ trans('lan_constant.id')}}</h5>
                                </div>
                                <div class="">

                                </div>
                            </div>
                            @if(!isset($data['login_with_token']))
                            <a href="{{url('/client_info_page')}}"
                                class="btn waves-effect waves-light btn-dark px-5 mx-2 float-right hibru">{{ trans('lan_constant.back_to_form')}}</a>
                            @endif
                        </div>
                        <div class="my-4">
                            <button type="button" class="btn waves-effect waves-light btn-info px-5 mx-2"
                                id="all_companies">{{ trans('lan_constant.activate_all_companies')}}</button>
                            <button type="button" class="btn waves-effect waves-light btn-info px-5 mx-2"
                                id="insurance_mountain_button"
                                company_name='insurance_mountain'>{{ trans('lan_constant.mount_of_insurance')}}</button>
                            <!--                            <button type="button"
                                class="btn waves-effect waves-light btn-info px-5 mx-2">{{ trans('lan_constant.all_the_mountains')}}</button>-->

                        </div>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>
                                            <h3>{{ trans('lan_constant.company')}}</h3>
                                        </th>
                                        <th>
                                            <h3>{{ trans('lan_constant.action')}}</h3>
                                        </th>
                                        <th style="width:30%;">
                                            <h3>{{trans('lan_constant.status')}}</h3>
                                        </th>
                                        <th>
                                            <h3>{{ trans('lan_constant.documents')}}</h3>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                    $ins_companies = array(
                                    'migdal',
                                    'clalbit',
                                    'menora',
                                    'phoenix' ,
                                    'harel' ,
                                    'ayalon',
                                    'haschara' ,
                                    'yashir',
                                    'nine_million',
                                    'aig',
                                    'insurance_mountain',
                                    //'all_mountain'
                                    );
                                    @endphp
                                    @foreach($ins_companies as $val)
                                    <tr>
                                        <td class="text-nowrap">
                                            <div class="last_field" company_name="{{$val}}">

                                                <img src="{{url('/assets/company_logo/Logo-'.$val.'.jpg')}}" alt="">
                                                <span class="set_font_bold">{{trans('lan_constant.'.$val)}}</span>
                                                <!-- <label class="custom_checkbox">
                                                <input type="checkbox">
                                                <span class="checkmark"></span>
                                            </label>-->
                                            </div>
                                        </td>
                                        <td class="text-nowrap">
                                            <a href="#" class="bug_icon hide mr-1" id="bug_{{$val}}"
                                                company_name="{{$val}}">
                                                <image src="{{url('/assets/icons/Bug.png')}}">
                                            </a>
                                            <button type="button"
                                                class="btn waves-effect waves-light btn-success px-5 get_captcha mr-1"
                                                company_name="{{$val}}"
                                                id="btn_{{$val}}">{{ trans('lan_constant.go_get_it')}}</button>

                                        </td>
                                        <td>
                                            <div class="table-loader" id="{{$val}}_loader" style="display: none">
                                                <img src="{{url('/assets/images/crawler_loader.gif')}}" alt="">
                                                <span class="loader_text" id="loader_text_{{$val}}"></span>
                                            </div>
                                            <p id="otp_details_{{$val}}"></p>

                                        </td>

                                        <td class="text-nowrap">
                                            <span class="set_text hide" id="{{$val}}_no_file_msg"></span>
                                            <a href="#" id="{{$val}}_file_link_zip" style="display: none"><img
                                                    src="{{url('/assets/icons/zip.png')}}" class="width-45"></a>
                                            <a href="#" id="{{$val}}_file_link_excel" style="display: none"><img
                                                    src="{{url('/assets/icons/icons8-xls-64.png')}}"
                                                    class="width-45"></a>
                                            <div id="show_error_image_{{$val}}" style="display:none">
                                            </div>   
                                                
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="myModal" class="modal-new">
    <div class="model-view">
        <span class="close close-icon">&times;</span>
        <img class="modal-content-new" id="img01">
    </div>
</div>

<!-- sample modal content -->
<div class="modal bs-example-modal-lg center_css" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
    aria-hidden="true" style="display: none;" id="fill_date_modal">
    <div class="modal-dialog modal-md modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="myLargeModalLabel"></h3>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form id="fill_date" name="fill_date">
                    <div class="row">
                        <div class="col-md-4">
                            <select class="custom-select col-12" id="year_new" name="year_new">
                                <option value="">{{ trans('lan_constant.a_year')}}</option>
                                @for ($a=2019;$a>=1919;$a--)
                                <option value="{{$a}}">{{$a}}</option>
                                @endfor
                            </select>
                        </div>
                        <div class="col-md-4">
                            <select class="custom-select col-12" id="month_new" name="month_new">
                                <option value="">{{ trans('lan_constant.a_month')}}</option>
                                @for($a=1;$a<=12;$a++) <option value="{{$a}}">{{$a}}</option>
                                    @endfor
                            </select>
                        </div>
                        <div class="col-md-4">
                            <select class="custom-select col-12" id="day_new" name="day_new">
                                <option value="">{{ trans('lan_constant.a_day')}}</option>
                                @for($a=1;$a<=31;$a++) <option value="{{$a}}">{{$a}}</option>
                                    @endfor
                            </select>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info" name="store_date" id="store_date"><i
                        class="fas fa-check"></i>
                    {{ trans('lan_constant.continued')}} </button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal bs-example-modal-lg center_css" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
    aria-hidden="true" style="display: none;" id="send_mail">
    <div class="modal-dialog modal-md modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="myLargeModalLabel"></h3>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form id="send_mail_form" name="send_mail_form">
                    <div class="row">
                        <div class="col-md-12">
                            <input type="hidden" id="company_name">
                            <div class="form-group">
                                <label>{{ trans('lan_constant.mail_popup_problem_lable')}} : </label>
                                <textarea class="form-control" rows="5" name="description" id="description"></textarea>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info" name="send_mail_button" id="send_mail_button"><i
                        class="fas fa-check"></i>
                    {{ trans('lan_constant.continued')}} </button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endsection
@section('pagecss')
@if(isset($data['login_with_token']))
<style>
    .left-sidebar {
        display: none;
    }

    .topbar {
        display: none;
    }

    .page-wrapper,
    .page-wrapper {
        padding-top: 30px;
    }

    .footer,
    .page-wrapper {
        margin-left: 0px;
        margin-right: 0px;
    }

    .footer {
        text-align: center;
    }
</style>
@endif
<link href="{{url('/assets/node_modules/sweetalert2/dist/sweetalert2.min.css')}}" rel="stylesheet">
<link href="{{url('/assets/node_modules/sweetalert2/dist/swal-forms.css')}}" rel="stylesheet">
@endsection
@section('pagescript')
<script src="{{url('/assets/node_modules/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script src="{{url('/js/jquery_validation/jquery.validate.js')}}"></script>
<script src="{{url('/js/jquery_validation/additional-methods.js')}}"></script>
<script src=".{{url('/assets/node_modules/sweetalert2/dist/sweetalert2.all.min.js')}}"></script>
<script src="{{url('/assets/node_modules/sweetalert2/swal-forms.js')}}"></script>
<script src="{{url('/assets/node_modules/sweetalert2/sweet-alert.init.js')}}"></script>
<script src="{{url('/js/lang.js?'.date('dmYhis'))}}"></script>
<script src="{{asset('js/new_crawler.js?'.date('dmYhis'))}}"></script>
@endsection