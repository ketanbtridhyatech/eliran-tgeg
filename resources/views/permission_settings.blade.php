@extends('layout.mainlayout')

@section('content')
<!-- ============================================================== -->
<!-- End Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">Permission settings </h4>
            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">{{ trans('lan_constant.home')}}</a></li>
                        <li class="breadcrumb-item active">Permission settings</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive m-t-40">
                            <table id="user_list" class="table display table-bordered table-striped no-wrap ">
                                <thead >
                                    <tr class="text-center">
                                        <th>Permission</th>
                                        @foreach($roles as $rl)
                                        <th>
                                        {{$rl->name}}
                                        </th>
                                        @endforeach
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!empty($permission))
                                    <?php $i=1; ?>
                                    @foreach($permission as $per)
                                    <tr class="text-center">
                                        <td>{{$per->name}}</td>
                                        @foreach($roles as $rl)
                                        @php
                                        $per_name = $per->id."__".$rl->id;
                                        @endphp
                                        <td>
                                            <div class="custom-control custom-checkbox mr-sm-2 mb-3 force_login">
                                                <input type="checkbox" class="custom-control-input add_remove_permission" id="role_{{$per->id."_".$rl->id}}" name="role_{{$per->id."_".$rl->id}}" value="true" role_id="{{$rl->id}}" permission_id="{{$per->id}}" {{in_array($per_name,$rp)?'checked':''}}>
                                                <label class="custom-control-label" for="role_{{$per->id."_".$rl->id}}"></label>
                                            </div>
                                        </td>
                                        @endforeach
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('pagecss')
<link rel="stylesheet" type="text/css" href="{{url('/assets/node_modules/datatables.net-bs4/css/dataTables.bootstrap4.css')}}">
<link rel="stylesheet" type="text/css" href="{{url('/assets/node_modules/datatables.net-bs4/css/responsive.dataTables.min.css')}}">
@endsection
@section('pagescript')
<script src="{{url('/js/jquery_validation/jquery.validate.js')}}"></script>
<script src="{{url('/js/jquery_validation/additional-methods.js')}}"></script>
<script src="{{url('/assets/node_modules/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{url('/assets/node_modules/datatables.net-bs4/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('js/user_permission.js')}}"></script>

<script>
        $(function () {
        @if(app()->getLocale() == 'en' && !empty(Session::has('success')))      
        $.toast({
            heading: 'Success',
            text: "{{Session::get('success')}}"
            , position: 'top-right'
            , loaderBg: '#ff6849'
            , icon: 'success'
            , hideAfter: 3500
            , stack: 6
        })
        @elseif(!empty(Session::has('success')))
        $.toast({
            heading: 'Success',
            text: "{{Session::get('success')}}"
            , position: 'top-left'
            , loaderBg: '#ff6849'
            , icon: 'success'
            , hideAfter: 3500
            , stack: 6
        })
        
        @endif
     
    })
        </script>
@endsection