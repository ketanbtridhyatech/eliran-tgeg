@extends('layout.mainlayout')

@section('content')
<!-- ============================================================== -->
<!-- End Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">{{trans('lan_constant.setting_title')}}</h4>
            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">{{ trans('lan_constant.home')}}</a>
                        </li>
                        <li class="breadcrumb-item active">{{ trans('lan_constant.setting_title')}}</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="col-sm-6 col-xs-6">
                            <form method="post" id="store_setting" name="store_setting"
                                action="{{url('/setting/store_setting')}}" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label>{{trans('lan_constant.logo_upload')}}</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">{{trans('lan_constant.upload')}}</span>
                                        </div>
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="logoUpload"
                                                name="logoUpload">
                                            <label class="custom-file-label" for="logoUpload">Choose file</label>
                                        </div>
                                    </div>
                                    <label id="logoUpload-error" class="error" for="logoUpload"></label>
                                    @if ($errors->has('logoUpload'))
                                    <span class="error">{{ $errors->first('logoUpload') }}</span>
                                    @endif<br>
                                    <span>
                                        <img src="{{asset('assets/images/'.$settings['site_logo'])}}"
                                            style="height: 50px;">
                                    </span>
                                </div>
                                <div class="form-group">
                                    <label>{{trans('lan_constant.title_label')}}</label>
                                    <input type="text" class="form-control" id="title_name" name="title_name"
                                        placeholder="Enter title name" value="{{$settings['title']}}">
                                    @if ($errors->has('title_name'))
                                    <span class="error">{{ $errors->first('title_name') }}</span>
                                    @endif
                                </div>
                                <!-- <div class="form-group">
                                    <label>{{trans('lan_constant.the_callback_return_url')}}</label>
                                    <input type="text" class="form-control" id="callback_return_url" name="callback_return_url" placeholder="Enter title name" value="{{$settings['callback_return_url']}}">
                                     @if ($errors->has('callback_return_url'))
                                        <span class="error">{{ $errors->first('callback_return_url') }}</span>
                                    @endif
                                </div>-->
                                <div class="form-group">
                                    <label>{{trans('lan_constant.python_url')}}</label>
                                    <input type="text" class="form-control" id="python_url" name="python_url"
                                        placeholder="Enter Python Url" value="{{$settings['python_url']}}">
                                    @if ($errors->has('python_url'))
                                    <span class="error">{{ $errors->first('python_url') }}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Email : </label>
                                    <input type="text" class="form-control" id="email" name="email"
                                        placeholder="Enter Email Address" value="{{$settings['email']}}">
                                    @if ($errors->has('email'))
                                    <span class="error">{{ $errors->first('email') }}</span>
                                    @endif
                                </div>

                                <div class="form-group">

                                    <label>Captcha Service : </label>

                                    <div class="custom-control custom-checkbox" id="remove_padding">
                                        <input class="form-check-input" type="radio" id="anticaptcha"
                                            value="anti_captcha" name="set_captcha"
                                            {{(!empty($settings) && $settings['captcha_service']=='anti_captcha')?'checked':''}}>
                                        <label class="form-check-label" for="anticaptcha" id="add_margin">
                                            Anticaptcha
                                        </label>
                                        <input class="form-check-input" type="radio" id="endcaptcha" value="end_captcha"
                                            name="set_captcha"
                                            {{(!empty($settings) && $settings['captcha_service']=='end_captcha')?'checked':''}}>
                                        <label class="form-check-label" for="endcaptcha">
                                            EndCaptcha
                                        </label>
                                    </div>
                                </div>



                                <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i>
                                    {{trans('lan_constant.add')}}</button>
                                <button type="button" class="btn btn-dark">Cancel</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

</script>
@endsection
@section('pagescript')
<script src="{{url('/js/jquery_validation/jquery.validate.js')}}"></script>
<script src="{{url('/js/jquery_validation/additional-methods.js')}}"></script>
<script src="{{url('/js/lang.js')}}"></script>
<script src="{{url('/js/setting.js')}}"></script>
<script>
    $(function () {
        @if(app()->getLocale() == 'en' && !empty(Session::has('success')))      
        $.toast({
            heading: 'Success',
            text: "{{Session::get('success')}}"
            , position: 'top-right'
            , loaderBg: '#ff6849'
            , icon: 'success'
            , hideAfter: 3500
            , stack: 6
        })
        @elseif(!empty(Session::has('error')))
        @if(app()->getLocale() == 'en')
        $.toast({
            heading: 'Error',
            text: "{{Session::get('error')}}"
            , position: 'top-right'
            , loaderBg: '#ff0000'
            , icon: 'error'
            , hideAfter: 3500
            , stack: 6
        })
        @else
        $.toast({
            heading: 'Error',
            text: "{{Session::get('error')}}"
            , position: 'top-left'
            , loaderBg: '#ff0000'
            , icon: 'error'
            , hideAfter: 3500
            , stack: 6
        })
        @endif
        @elseif(!empty(Session::has('success')))
        $.toast({
            heading: 'Success',
            text: "{{Session::get('success')}}"
            , position: 'top-left'
            , loaderBg: '#ff6849'
            , icon: 'success'
            , hideAfter: 3500
            , stack: 6
        })
        
        @endif
     
    })
</script>
@endsection