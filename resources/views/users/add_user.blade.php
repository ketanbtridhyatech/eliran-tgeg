@extends('layout.mainlayout')

@section('content')
<!-- ============================================================== -->
<!-- End Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <!-- <h4 class="text-themecolor">{{ trans('lan_constant.dashboard')}}</h4>-->
                <h4 class="text-themecolor">
                    {{isset($action) && $action == 'edit' ?  trans('lan_constant.update_user') : trans('lan_constant.add_user') }}
                </h4>

            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">{{ trans('lan_constant.home')}}</a>
                        </li>
                        <li class="breadcrumb-item active">
                            {{isset($action) && $action == 'edit' ?  trans('lan_constant.update_user') : trans('lan_constant.add_user') }}
                        </li>
                    </ol>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card card-body">
                    <div class="row">
                        <div class="col-sm-6 col-xs-6">
                            <form method="post" id="store_user_data" name="store_user_data"
                                action="{{url('/user/store_user_details')}}">
                                @csrf
                                @if(isset($action) && $action == 'edit')
                                <input type="hidden" name="user_id" value="{{$user_details->id}}">
                                <div class="form-group">
                                    <label for="role">{{trans('lan_constant.role')}} <span
                                            class="error">*</span></label>
                                    <select id="role" name="role" class="form-control">
                                        <option value="">{{trans('lan_constant.select_role')}}</option>
                                        @foreach($roles as $role)
                                        <option value="{{$role->id}}"
                                            {{(!empty($role) && in_array($role->id,$user_role))?'selected':''}}>
                                            {{$role->name}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('role'))
                                    <span class="error">{{ $errors->first('role') }}</span>
                                    @endif
                                </div>
                                <div class="form-group" id="show_hide_company">
                                <div class="form-group">
                                    <label for="role">{{trans('lan_constant.company')}} <span
                                            class="error">*</span></label>
                                    <select id="company_id" name="company_id" class="form-control">
                                        <option value="">{{trans('lan_constant.select_company')}}</option>
                                        @foreach($company_role_user_details as $company)
                                        <option value="{{$company->id}}"
                                            {{(!empty($user_details) && $user_details->company_id == $company->id)?'selected':''}}>
                                            {{$company->first_name." ".$company->last_name}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('company_id'))
                                    <span class="error">{{ $errors->first('company_id') }}</span>
                                    @endif
                                </div>
                            </div>
                                <div class="form-group">
                                    <label for="first_name">{{trans('lan_constant.first_name')}} <span
                                            class="error">*</span></label>
                                    <input type="text" class="form-control" id="first_name" name="first_name"
                                        placeholder="{{trans('lan_constant.enter_first_name')}}"
                                        value="{{$user_details->first_name}}">
                                    @if ($errors->has('first_name'))
                                    <span class="error">{{ $errors->first('first_name') }}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="last_name">{{trans('lan_constant.last_name')}} <span
                                            class="error">*</span></label>
                                    <input type="text" class="form-control" id="last_name" name="last_name"
                                        placeholder="{{trans('lan_constant.enter_last_name')}}"
                                        value="{{$user_details->last_name}}">
                                    @if ($errors->has('last_name'))
                                    <span class="error">{{ $errors->first('last_name') }}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="email">{{trans('lan_constant.email')}} <span
                                            class="error">*</span></label>
                                    <input type="text" class="form-control" id="email" name="email"
                                        placeholder="{{trans('lan_constant.enter_email')}}"
                                        value="{{$user_details->email}}">
                                    @if ($errors->has('email'))
                                    <span class="error">{{ $errors->first('email') }}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="phone_number">{{trans('lan_constant.phone')}}</label>
                                    <input type="text" class="form-control" id="phone_number" name="phone_number"
                                        placeholder="{{trans('lan_constant.enter_phone_number')}}"
                                        value="{{$user_details->phone_number}}">
                                </div>
                                <div id="show_hide_details_for_company">
                                    <div class="form-group">
                                        <label for="s3_bucket_name">{{trans('lan_constant.s3_bucket_name')}}</label>
                                        <input type="text" class="form-control" id="s3_bucket_name"
                                            name="s3_bucket_name"
                                            placeholder="{{trans('lan_constant.s3_bucket_name')}}" value="{{$user_details->s3_bucket_name}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="s3_access_key">{{trans('lan_constant.s3_access_key')}}</label>
                                        <input type="text" class="form-control" id="s3_access_key" name="s3_access_key"
                                            placeholder="{{trans('lan_constant.s3_access_key')}}" value="{{$user_details->s3_access_key}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="s3_secret_key">{{trans('lan_constant.s3_secret_key')}}</label>
                                        <input type="text" class="form-control" id="s3_secret_key" name="s3_secret_key"
                                            placeholder="{{trans('lan_constant.s3_secret_key')}}" value="{{$user_details->s3_secret_key}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="ftp_host">{{trans('lan_constant.ftp_host')}}</label>
                                        <input type="text" class="form-control" id="ftp_host" name="ftp_host"
                                            placeholder="{{trans('lan_constant.ftp_host')}}" value="{{$user_details->ftp_host}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="ftp_password">{{trans('lan_constant.ftp_password')}}</label>
                                        <input type="text" class="form-control" id="ftp_password" name="ftp_password"
                                            placeholder="{{trans('lan_constant.ftp_password')}}" value="{{$user_details->ftp_password}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="ftp_port">{{trans('lan_constant.ftp_port')}}</label>
                                        <input type="text" class="form-control" id="ftp_port" name="ftp_port"
                                            placeholder="{{trans('lan_constant.ftp_port')}}"  value="{{$user_details->ftp_port}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="ftp_username">{{trans('lan_constant.ftp_username')}}</label>
                                        <input type="text" class="form-control" id="ftp_username" name="ftp_username"
                                            placeholder="{{trans('lan_constant.ftp_username')}}" value="{{$user_details->ftp_username}}" >
                                    </div>
                                    <div class="form-group">
                                        <label for="ftp_public_url">{{trans('lan_constant.ftp_public_url')}}</label>
                                        <input type="text" class="form-control" id="ftp_public_url"
                                            name="ftp_public_url"
                                            placeholder="{{trans('lan_constant.ftp_public_url')}}" value="{{$user_details->ftp_public_url}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="webhook_url">{{trans('lan_constant.webhook_url')}}</label>
                                        <input type="text" class="form-control" id="webhook_url"
                                            name="webhook_url"
                                            placeholder="{{trans('lan_constant.webhook_url')}}" value="{{$user_details->webhook_url}}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="ip">{{trans('lan_constant.ip_label')}}</label>
                                    <input type="text" class="form-control" id="ip_address" name="ip_address"
                                        placeholder="{{trans('lan_constant.enter_ip_address')}}"
                                        value="{{$user_details->ip}}">
                                </div>
                                <div class="form-group">
                                    <label for="secure_token">{{trans('lan_constant.secure_token_label')}}</label>
                                    <input type="text" class="form-control" id="secure_token" name="secure_token"
                                        placeholder="{{trans('lan_constant.enter_secure_token')}}"
                                        value="{{$user_details->s_token}}">
                                </div>

                                @else
                                <div class="form-group">
                                    <label for="role">{{trans('lan_constant.role')}} <span
                                            class="error">*</span></label>
                                    <select id="role" name="role" class="form-control">
                                        <option value="">{{trans('lan_constant.select_role')}}</option>
                                        @foreach($roles as $role)
                                        <option value="{{$role->id}}">{{$role->name}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('role'))
                                    <span class="error">{{ $errors->first('role') }}</span>
                                    @endif
                                </div>
                                <div class="form-group" id="show_hide_company">
                                    <label for="role">{{trans('lan_constant.company')}} <span
                                            class="error">*</span></label>
                                    <select id="company_id" name="company_id" class="form-control">
                                        <option value="">{{trans('lan_constant.select_company')}}</option>
                                        @foreach($company_role_user_details as $company)
                                        <option value="{{$company->id}}">
                                            {{$company->first_name." ".$company->last_name}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('company_id'))
                                    <span class="error">{{ $errors->first('company_id') }}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="first_name">{{trans('lan_constant.first_name')}} <span
                                            class="error">*</span></label>
                                    <input type="text" class="form-control" id="first_name" name="first_name"
                                        placeholder="{{trans('lan_constant.enter_first_name')}}"
                                        value="{{old('first_name')}}">
                                    @if ($errors->has('first_name'))
                                    <span class="error">{{ $errors->first('first_name') }}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="last_name">{{trans('lan_constant.last_name')}} <span
                                            class="error">*</span></label>
                                    <input type="text" class="form-control" id="last_name" name="last_name"
                                        placeholder="{{trans('lan_constant.enter_last_name')}}"
                                        value="{{old('last_name')}}">
                                    @if ($errors->has('last_name'))
                                    <span class="error">{{ $errors->first('last_name') }}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="email">{{trans('lan_constant.email')}} <span
                                            class="error">*</span></label>
                                    <input type="text" class="form-control" id="email" name="email"
                                        placeholder="{{trans('lan_constant.enter_email')}}" value="{{old('email')}}">
                                    @if ($errors->has('email'))
                                    <span class="error">{{ $errors->first('email') }}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="email">{{trans('lan_constant.phone')}}</label>
                                    <input type="text" class="form-control" id="phone_number" name="phone_number"
                                        placeholder="{{trans('lan_constant.enter_phone_number')}}"
                                        value="{{old('phone_number')}}">
                                </div>
                                <div class="form-group">
                                    <label for="password">{{trans('lan_constant.password')}} <span
                                            class="error">*</span></label>
                                    <input type="password" class="form-control" id="password" name="password"
                                        placeholder="{{trans('lan_constant.enter_password')}}">
                                    @if ($errors->has('password') && $errors->first('password') != 'The password
                                    confirmation does not match.')
                                    <span class="error">{{ $errors->first('password') }}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="confirm_password">{{trans('lan_constant.confirm_password')}} <span
                                            class="error">*</span></label>
                                    <input type="password" class="form-control" id="confirm_password"
                                        name="confirm_password"
                                        placeholder="{{trans('lan_constant.enter_confirm_password')}}">
                                    @if ($errors->has('confirm_password'))
                                    <span class="error">{{ $errors->first('confirm_password') }}</span>
                                    @elseif($errors->has('password') && $errors->first('password') == 'The password
                                    confirmation does not match.')
                                    <span class="error">{{ $errors->first('password') }}</span>
                                    @endif
                                </div>
                                <div id="show_hide_details_for_company">
                                    <div class="form-group">
                                        <label for="s3_bucket_name">{{trans('lan_constant.s3_bucket_name')}}</label>
                                        <input type="text" class="form-control" id="s3_bucket_name"
                                            name="s3_bucket_name"
                                            placeholder="{{trans('lan_constant.s3_bucket_name')}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="s3_access_key">{{trans('lan_constant.s3_access_key')}}</label>
                                        <input type="text" class="form-control" id="s3_access_key" name="s3_access_key"
                                            placeholder="{{trans('lan_constant.s3_access_key')}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="s3_secret_key">{{trans('lan_constant.s3_secret_key')}}</label>
                                        <input type="text" class="form-control" id="s3_secret_key" name="s3_secret_key"
                                            placeholder="{{trans('lan_constant.s3_secret_key')}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="ftp_host">{{trans('lan_constant.ftp_host')}}</label>
                                        <input type="text" class="form-control" id="ftp_host" name="ftp_host"
                                            placeholder="{{trans('lan_constant.ftp_host')}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="ftp_password">{{trans('lan_constant.ftp_password')}}</label>
                                        <input type="text" class="form-control" id="ftp_password" name="ftp_password"
                                            placeholder="{{trans('lan_constant.ftp_password')}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="ftp_port">{{trans('lan_constant.ftp_port')}}</label>
                                        <input type="text" class="form-control" id="ftp_port" name="ftp_port"
                                            placeholder="{{trans('lan_constant.ftp_port')}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="ftp_username">{{trans('lan_constant.ftp_username')}}</label>
                                        <input type="text" class="form-control" id="ftp_username" name="ftp_username"
                                            placeholder="{{trans('lan_constant.ftp_username')}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="ftp_public_url">{{trans('lan_constant.ftp_public_url')}}</label>
                                        <input type="text" class="form-control" id="ftp_public_url"
                                            name="ftp_public_url"
                                            placeholder="{{trans('lan_constant.ftp_public_url')}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="webhook_url">{{trans('lan_constant.webhook_url')}}</label>
                                        <input type="text" class="form-control" id="webhook_url"
                                            name="webhook_url"
                                            placeholder="{{trans('lan_constant.webhook_url')}}">
                                    </div>
                                </div>
                                @endif

                                <button type="submit" class="btn btn-success mr-2" name="add_user"
                                    id="add_user">{{isset($action) && $action == 'edit'?trans('lan_constant.update'):trans('lan_constant.add')}}</button>
                                <button type="button" class="btn btn-dark"
                                    onClick="location.href='{{url('/users')}}'">Cancel</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('pagecss')
<link href="{{url('/assets/node_modules/bootstrap-tagsinput/dist/bootstrap-tagsinput.css')}}" rel="stylesheet" />

@endsection
@section('pagescript')
<script src="{{url('/js/jquery_validation/jquery.validate.js')}}"></script>
<script src="{{url('/js/jquery_validation/additional-methods.js')}}"></script>
<script src="{{url('/assets/node_modules/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{url('/assets/node_modules/datatables.net-bs4/js/dataTables.responsive.min.js')}}"></script>
<script src="{{url('/assets/node_modules/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js')}}"></script>

<script src="{{url('/js/user.js?'.date('Ymdhis'))}}"></script>
@endsection