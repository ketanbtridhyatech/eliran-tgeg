@extends('layout.mainlayout')

@section('content')
<!-- ============================================================== -->
<!-- End Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">{{ trans('lan_constant.users_list')}}</h4>
            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">{{ trans('lan_constant.home')}}</a>
                        </li>
                        <li class="breadcrumb-item active">{{ trans('lan_constant.users')}}</li>
                    </ol>
                    <button type="button" class="btn btn-info d-none d-lg-block m-l-15" id="add_user_button"><i
                            class="fa fa-plus-circle"></i> {{ trans('lan_constant.add_new_user')}}</button>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive m-t-40">
                            <table id="user_list" class="table display table-bordered table-striped no-wrap">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{ trans('lan_constant.first_name')}}</th>
                                        <th>{{ trans('lan_constant.last_name')}}</th>
                                        <th>{{ trans('lan_constant.company')}}</th>
                                        <th>{{ trans('lan_constant.email')}}</th>
                                        <th>{{ trans('lan_constant.phone')}}</th>
                                        <th>{{ trans('lan_constant.action')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!empty($users))
                                    <?php $i=1; ?>
                                    @foreach($users as $user)
                                    <tr>
                                        <td>{{$i}}</td>
                                        <td>{{$user->first_name}}</td>
                                        <td>{{$user->last_name}}</td>
                                        <td>{{$user->company_name}}</td>
                                        <td>{{$user->email}}</td>
                                        <td>{{$user->phone_number}}</td>
                                        <td>
                                            @if(!empty($user->company_id))
                                            <a class="btn btn-secondary copy_url" data-clipboard-text="{{url('/show_companies?token='.$user->company_token)}}" href="javascript:" type="button" name="copy_url" aria-label="Copy" title="Copy" id="copy_url" aid="{{$user->id}}"><i
                                                    class="fa fas fa-lock"></i></a>
                                            @endif
                                            <a class="btn btn-secondary" href="{{url('users/edit/'.$user->id)}}"
                                                type="button" name="edit" aria-label="Edit" title="Edit"><i
                                                    class="fa fas fa-edit"></i></a>
                                            <a class="btn btn-secondary sa-confirm" type="button" name="delete"
                                                aria-label="Delete" title="Delete" id="delete"
                                                user_id="{{$user->id}}"><i class="fa fas fa-trash"></i></a>
                                        </td>
                                        <?php
                                        $i++;
                                        ?>
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('pagecss')
<link rel="stylesheet" type="text/css"
    href="{{url('/assets/node_modules/datatables.net-bs4/css/dataTables.bootstrap4.css')}}">
<link rel="stylesheet" type="text/css"
    href="{{url('/assets/node_modules/datatables.net-bs4/css/responsive.dataTables.min.css')}}">
@endsection
@section('pagescript')
<script src="{{url('/js/jquery_validation/jquery.validate.js')}}"></script>
<script src="{{url('/js/jquery_validation/additional-methods.js')}}"></script>
<script src="{{url('/assets/node_modules/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{url('/assets/node_modules/datatables.net-bs4/js/dataTables.responsive.min.js')}}"></script>
<script src="{{url('/js/user.js?'.date('dmYhis'))}}"></script>
<script src="{{url('/js/lang.js?'.date('dmYhis'))}}"></script>
<script>
    $(function () {
        @if(app()->getLocale() == 'en' && !empty(Session::has('success')))      
        $.toast({
            heading: 'Success',
            text: "{{Session::get('success')}}"
            , position: 'top-right'
            , loaderBg: '#ff6849'
            , icon: 'success'
            , hideAfter: 3500
            , stack: 6
        })
        @elseif(!empty(Session::has('success')))
        $.toast({
            heading: 'Success',
            text: "{{Session::get('success')}}"
            , position: 'top-left'
            , loaderBg: '#ff6849'
            , icon: 'success'
            , hideAfter: 3500
            , stack: 6
        })
        
        @endif
     
    })
</script>
@endsection