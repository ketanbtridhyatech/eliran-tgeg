<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\CrawlersController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;


Auth::routes();
Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('config:cache');
    // return what you want
});
Route::get('/', 'HomeController@index')->name('home');
Route::get('/dashboard', 'HomeController@index')->name('home');
Route::get('/logout', 'HomeController@logout')->name('logout');
Route::post('/change_language','HomeController@change_language')->name('change_language');
Route::get('/users','UserController@index')->name('user_list');
Route::get('/users/add_user','UserController@add_new_user')->name('add_new_user');
Route::get('/users/edit/{id}','UserController@edit_user')->name('edit_user');
Route::post('/user/store_user_details','UserController@store_user_details')->name('store_user_details');
Route::delete('/user/delete','UserController@delete_user')->name('delete_user');
Route::post('/get_otp','CrawlersController@get_otp')->name('get_otp');
Route::post('/verify_otp','CrawlersController@verify_otp')->name('verify_otp');
Route::post('/get_files','CrawlersController@get_files')->name('get_files');
Route::post('/money_mountain_zip_download','CrawlersController@money_mountain_zip_download')->name('money_mountain_zip_download');
Route::get('/permission_settings','HomeController@permission_settings')->name('permission_settings');
Route::post('/add_remove_permission','HomeController@add_remove_permission')->name('add_remove_permission');
Route::post('/insurance_mountain_process','CrawlersController@insurance_mountain_process')->name('insurance_mountain_process');

//Activity log listing
Route::get('/activity/logs','ActivityLogController@index')->name('log_list');
Route::get('/new_client','HomeController@newClient')->name('newClient');
Route::get('/client_info_page','HomeController@client_info_page')->name('client_info_page');
// Localization
Route::get('/js/lang.js', function () {
    Cache::forget('lang.js');
    $strings = Cache::rememberForever('lang.js', function () {
        $lang = config('app.locale');

        $files   = glob(resource_path('lang/' . $lang . '/*.php'));
        $strings = [];

        foreach ($files as $file) {
            $name           = basename($file, '.php');
            $strings[$name] = require $file;
        }

        return $strings;
    });

    header('Content-Type: text/javascript');
    echo('window.i18n = ' . json_encode($strings) . ';');
    exit();
})->name('assets.lang');

$request=Request::all();
if(!empty($request['token']) && !empty($request['agent_id']) ){
    Auth::logout();
    $action = 'Auth\LoginController@login';
    
}else{
    $action = 'CrawlersController@show_companies';
}
Route::get('/show_companies',$action)->name('company_list');
Route::get('/general_setting_page','SettingsController@update_settings')->name('general_setting_page');
Route::post('/setting/store_setting','SettingsController@store_setting');
Route::get('/direct_login','Auth\LoginController@login');
Route::get('/dashboard/chart_data','HomeController@chartData');
Route::get('/billing/billing_report','CrawlersController@billing_report');
Route::post('/hit_counter/filter','CrawlersController@billing_report_filter');
Route::post('/dashboard/hit_counter_report','HomeController@hit_counter_report');
Route::post('/send_mail','CrawlersController@send_mail');
Route::get('/activity/investigations_log','CrawlersController@investigations_log');
Route::post('/investigations_log/filter','CrawlersController@investigations_log_filter');
Route::post('/investigations_log/print_json','CrawlersController@print_json');
Route::get('/clear_browser_instance','Auth\LoginController@clear_browser_instance');




